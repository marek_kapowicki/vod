package infrastructure.error;

import org.junit.Test;
import pl.marekk.rental.domain.client.order.ItemAlreadyReturnedException;
import pl.marekk.rental.domain.client.order.OrderItemTurnException;

import static org.assertj.core.api.BDDAssertions.then;


public class ErrorInfoFactoryTest {
    ErrorInfoFactory factory = new ErrorInfoFactory();

    @Test
    public void shouldReturnGeneralCodeForUnknownException() throws Exception {
        //given
        Exception e = new RuntimeException("test");
        //when
        ErrorInfo errorInfo = factory.create(e);
        //then
        then(errorInfo.getCode()).isEqualTo(ErrorCode.GENERAL.getValue());
    }

    @Test
    public void shouldReturnITEM_ALREADY_RETURNEDCodeForItemAlreadyReturnedException() throws Exception {
        //given
        Exception e = new ItemAlreadyReturnedException();
        //when
        ErrorInfo errorInfo = factory.create(e);
        //then
        then(errorInfo.getCode()).isEqualTo(ErrorCode.ITEM_ALREADY_RETURNED.getValue());
    }

    @Test
    public void shouldReturnILLEGAL_ARGCodeForIllegalArgumentException() throws Exception {
        //given
        Exception e = new IllegalArgumentException();
        //when
        ErrorInfo errorInfo = factory.create(e);
        //then
        then(errorInfo.getCode()).isEqualTo(ErrorCode.ILLEGAL_ARG.getValue());
    }

    @Test
    public void shouldReturnTURNING_ITEM_PROBLEMCodeForOrderItemTurnException() throws Exception {
        //given
        Exception e = new OrderItemTurnException("test issue");
        //when
        ErrorInfo errorInfo = factory.create(e);
        //then
        then(errorInfo.getCode()).isEqualTo(ErrorCode.TURNING_ITEM_PROBLEM.getValue());
    }
}
