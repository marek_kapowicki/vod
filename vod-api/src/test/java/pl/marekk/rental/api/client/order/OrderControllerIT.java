package pl.marekk.rental.api.client.order;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import configuration.TestApplication;
import ddd.support.query.IdResponse;
import infrastructure.ApiVersions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.marekk.rental.api.CurrencyRepresentation;
import pl.marekk.rental.application.OrderManagementService;
import pl.marekk.rental.domain.client.order.OrderDto;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestApplication.class)
@WebIntegrationTest(randomPort = true)
@Transactional
public class OrderControllerIT {


    private MockMvc mockMvc;

    @Inject
    private WebApplicationContext wac;

    @Inject
    private OrderManagementService orderManagementService;

    @Inject
    private ObjectMapper objectMapper;

    @Before
    public void init() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .defaultRequest(get("/")
                        .contentType(ApiVersions.ORDER_JSON_VERSION_1)
                        .accept(ApiVersions.ORDER_JSON_VERSION_1)
                ).build();

    }

    @Test
    public void shouldNotCreateOrderBaseOnWrongRequest() throws Exception {
        String createOrderRequest = objectMapper.writeValueAsString(new CreateOrderRequest(CurrencyRepresentation.EUR, new HashMap<>()));
        mockMvc.perform(post("/orders").content(createOrderRequest))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void shouldCreateOrder() throws Exception {
        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put("movie_1", 2L)
                .build();
        String createOrderRequest = objectMapper.writeValueAsString(new CreateOrderRequest(CurrencyRepresentation.EUR, positions));
        mockMvc.perform(post("/orders").content(createOrderRequest))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(status().isCreated());
    }


    @Test
    public void shouldFindOrderById() throws Exception {
        //given
        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put("movie_1", 2L)
                .build();
        String createOrderRequest = objectMapper.writeValueAsString(
                new CreateOrderRequest(CurrencyRepresentation.PLN, positions));
        String orderJsonId = mockMvc.perform(post("/orders").content(createOrderRequest)).andReturn()
                .getResponse().getContentAsString();
        String orderId = objectMapper.readValue(orderJsonId, IdResponse.class).getId();
        //when
        mockMvc.perform(get("/orders/" + orderId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.additionalPrice").value("0.00 PLN"))
                .andExpect(jsonPath("$.orderStatus").value("IN_PROGRESS"))
                .andExpect(jsonPath("$.items", hasSize(1)));

    }

    @Test
    public void shouldTurnOrderItem() throws Exception {
        //given
        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put("movie_1", 2L)
                .build();
        String createOrderRequest = objectMapper.writeValueAsString(
                new CreateOrderRequest(CurrencyRepresentation.PLN, positions));
        String orderJsonId = mockMvc.perform(post("/orders").content(createOrderRequest)).andReturn()
                .getResponse().getContentAsString();
        String orderId = objectMapper.readValue(orderJsonId, IdResponse.class).getId();
        String jsonOrder = mockMvc.perform(get("/orders/" + orderId)).andReturn().getResponse().getContentAsString();
        OrderDto orderDto = objectMapper.readValue(jsonOrder, OrderDto.class);
        //when
        String orderItemId = orderDto.getItems().get(0).getId();
        mockMvc.perform(put("/orders/" + orderId + "/item/" + orderItemId))
                .andExpect(status().isOk());

    }


    @Test
    public void shouldTurnOrder() throws Exception {
        //given
        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put("movie_1", 2L)
                .build();
        String createOrderRequest = objectMapper.writeValueAsString(
                new CreateOrderRequest(CurrencyRepresentation.PLN, positions));
        String orderJsonId = mockMvc.perform(post("/orders").content(createOrderRequest)).andReturn()
                .getResponse().getContentAsString();
        String orderId = objectMapper.readValue(orderJsonId, IdResponse.class).getId();
        //when
        mockMvc.perform(put("/orders/" + orderId))
                .andExpect(status().isOk());

    }


}
