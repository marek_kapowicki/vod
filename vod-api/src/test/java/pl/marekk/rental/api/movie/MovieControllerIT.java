package pl.marekk.rental.api.movie;

import configuration.TestApplication;
import infrastructure.ApiVersions;
import infrastructure.error.ErrorCode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.inject.Inject;
import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestApplication.class)
@WebIntegrationTest(randomPort = true)
@Transactional
public class MovieControllerIT {

    private MockMvc mockMvc;

    @Inject
    private WebApplicationContext wac;


    @Before
    public void init() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .defaultRequest(get("/")
                        .contentType(ApiVersions.MOVIE_JSON_VERSION_1)
                        .accept(ApiVersions.MOVIE_JSON_VERSION_1)
                ).build();
    }

    @Test
    public void shouldFindMovieById() throws Exception {
        mockMvc.perform(get("/movies/movie_1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("scarface"))
                .andExpect(jsonPath("$.description").value("word of Tony Montana"))
                .andExpect(jsonPath("$.category").value("OLD"))
                .andExpect(jsonPath("$.grade").value("CRIME"))
                .andExpect(jsonPath("$.id").value("movie_1"));
    }

    @Test
    public void shouldFindMovieAll() throws Exception {
        mockMvc.perform(get("/movies?page=0&limit=5"))
                .andExpect(status().isOk());

    }

    @Test
    public void shouldNotAcceptOtherType() throws Exception {
        mockMvc.perform(get("/movies?page=0&limit=5").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.code").value(ErrorCode.ILLEGAL_ARG.getValue()));


    }
}
