package pl.marekk.rental.api.client.order;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import configuration.TestApplication;
import ddd.support.query.IdResponse;
import infrastructure.ApiVersions;
import infrastructure.error.ErrorCode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.marekk.rental.api.CurrencyRepresentation;
import pl.marekk.rental.domain.client.order.OrderDto;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestApplication.class)
@WebIntegrationTest(randomPort = true)
@Transactional
public class OrderErrorHandlingIT {

    @Inject
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Inject
    private ObjectMapper objectMapper;


    @Before
    public void init() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .defaultRequest(get("/")
                        .contentType(ApiVersions.ORDER_JSON_VERSION_1)
                        .accept(ApiVersions.ORDER_JSON_VERSION_1)).build();
    }

    @Test
    public void findByIdReturns204WhenOrderDoesNotExists() throws Exception {
        mockMvc.perform(get("/orders/not_exists")).
                andExpect(status().isNoContent());
    }

    @Test
    public void findAllReturns204WhenOrderDoesNotExists() throws Exception {
        mockMvc.perform(get("/orders/not_exists")).
                andExpect(status().isNoContent());
    }

    @Test
    public void throwExceptionDuringCreateOrderWhenMovieDoesNotExists() throws Exception {
        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put("not_existed_movie", 2L)
                .build();
        String createOrderRequest = objectMapper.writeValueAsString(new CreateOrderRequest(CurrencyRepresentation.EUR, positions));
        mockMvc.perform(post("/orders").content(createOrderRequest))
                .andExpect(jsonPath("$.code").value(ErrorCode.MOVIE_NOT_EXISTS.getValue()))
                .andExpect(jsonPath("$.message").value("requested ids=[not_existed_movie] does not exists"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void turnItemThrowsExceptionWhenOrderDoesNotExists() throws Exception {
        //given
        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put("movie_1", 2L)
                .build();
        String createOrderRequest = objectMapper.writeValueAsString(
                new CreateOrderRequest(CurrencyRepresentation.PLN, positions));
        String orderJsonId = mockMvc.perform(post("/orders").content(createOrderRequest)).andReturn()
                .getResponse().getContentAsString();
        String orderId = objectMapper.readValue(orderJsonId, IdResponse.class).getId();
        //when
        mockMvc.perform(put("/orders/" + orderId + "/item/" + "wrong_item"))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.code").value(ErrorCode.TURNING_ITEM_PROBLEM.getValue()));
    }

    @Test
    public void shouldThrowExceptionDuringTurnReturningItem() throws Exception {
        //given
        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put("movie_1", 2L)
                .build();
        String createOrderRequest = objectMapper.writeValueAsString(
                new CreateOrderRequest(CurrencyRepresentation.PLN, positions));
        String orderJsonId = mockMvc.perform(post("/orders").content(createOrderRequest)).andReturn()
                .getResponse().getContentAsString();
        String orderId = objectMapper.readValue(orderJsonId, IdResponse.class).getId();
        String jsonOrder = mockMvc.perform(get("/orders/" + orderId)).andReturn().getResponse().getContentAsString();
        OrderDto orderDto = objectMapper.readValue(jsonOrder, OrderDto.class);
        //when
        String orderItemId = orderDto.getItems().get(0).getId();
        mockMvc.perform(put("/orders/" + orderId + "/item/" + orderItemId));
        mockMvc.perform(put("/orders/" + orderId + "/item/" + orderItemId))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.code").value(ErrorCode.ITEM_ALREADY_RETURNED.getValue()));

    }
}
