package infrastructure;


public class ApiVersions {

    public static final String MOVIE_JSON_VERSION_1 = "application/vnd.rental.movie.v1+json";

    public static final String CLIENT_JSON_VERSION_1 = "application/vnd.rental.client.v1+json";

    public static final String ORDER_JSON_VERSION_1 = "application/vnd.rental.order.v1+json";
}
