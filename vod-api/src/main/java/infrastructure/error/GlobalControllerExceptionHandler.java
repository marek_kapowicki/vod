package infrastructure.error;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.marekk.rental.domain.client.order.ItemAlreadyReturnedException;
import pl.marekk.rental.domain.client.order.OrderItemTurnException;
import pl.marekk.rental.domain.movie.RequestedMovieDoesNotExists;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler {

    private final ErrorInfoFactory errorInfoFactory;

    @Inject
    public GlobalControllerExceptionHandler(ErrorInfoFactory errorInfoFactory) {
        this.errorInfoFactory = errorInfoFactory;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({Exception.class})
    @ResponseBody
    public ErrorInfo handleError(HttpServletRequest request, Exception exception) {
        log.error("Unhandled error for request: {}", request.getRequestURI(), exception);
        return errorInfoFactory.create(exception);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ExceptionHandler({EntityNotFoundException.class})
    @ResponseBody
    public ErrorInfo entityNotFound(HttpServletRequest request, EntityNotFoundException exception) {
        log.error("object not found: {}", request.getRequestURI(), exception);
        return null;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ItemAlreadyReturnedException.class})
    @ResponseBody
    public ErrorInfo itemAlreadyTurned(HttpServletRequest request, ItemAlreadyReturnedException exception) {
        log.warn("item already exists error: {}", request.getRequestURI(), exception);
        return errorInfoFactory.create(exception);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({OrderItemTurnException.class})
    @ResponseBody
    public ErrorInfo turnException(HttpServletRequest request, OrderItemTurnException exception) {
        log.error("error occurs during turn item: {}", request.getRequestURI(), exception);
        return errorInfoFactory.create(exception);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({RequestedMovieDoesNotExists.class})
    @ResponseBody
    public ErrorInfo requestedMovieDoesNotExists(HttpServletRequest request, RequestedMovieDoesNotExists exception) {
        log.error("requested movies don't exists {}", request.getRequestURI(), exception);
        return errorInfoFactory.create(exception);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({
            IllegalArgumentException.class,
            MethodArgumentNotValidException.class,
            HttpMediaTypeNotSupportedException.class
    })
    @ResponseBody
    public ErrorInfo wrongRequest(HttpServletRequest request, Exception exception) {
        log.warn("wrong request {}", request, exception);
        return errorInfoFactory.create(exception);
    }


}
