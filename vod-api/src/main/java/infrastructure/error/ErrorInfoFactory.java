package infrastructure.error;

import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import pl.marekk.rental.domain.client.order.ItemAlreadyReturnedException;
import pl.marekk.rental.domain.client.order.OrderItemTurnException;
import pl.marekk.rental.domain.movie.RequestedMovieDoesNotExists;

import javax.inject.Named;
import javax.persistence.EntityNotFoundException;

import static com.nurkiewicz.typeof.TypeOf.whenTypeOf;

@Named
public class ErrorInfoFactory {

    public ErrorInfo create(Exception ex) {
        return new ErrorInfo(extractCode(ex).getValue(), ex.getMessage());
    }

    private ErrorCode extractCode(Exception ex) {
        return whenTypeOf(ex)
                .is(RequestedMovieDoesNotExists.class).thenReturn(ErrorCode.MOVIE_NOT_EXISTS)
                .is(IllegalArgumentException.class).thenReturn(ErrorCode.ILLEGAL_ARG)
                .is(HttpMediaTypeNotSupportedException.class).thenReturn(ErrorCode.ILLEGAL_ARG)
                .is(MethodArgumentNotValidException.class).thenReturn(ErrorCode.ILLEGAL_ARG)
                .is(OrderItemTurnException.class).thenReturn(ErrorCode.TURNING_ITEM_PROBLEM)
                .is(EntityNotFoundException.class).thenReturn(ErrorCode.NOT_EXISTING)
                .is(ItemAlreadyReturnedException.class).thenReturn(ErrorCode.ITEM_ALREADY_RETURNED)
                .orElse(ErrorCode.GENERAL);
    }
}
