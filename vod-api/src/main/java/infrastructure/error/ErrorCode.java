package infrastructure.error;

public enum ErrorCode {

    MOVIE_NOT_EXISTS(94),
    TURNING_ITEM_PROBLEM(95),
    ITEM_ALREADY_RETURNED(96),
    NOT_EXISTING(97),
    ILLEGAL_ARG(98),
    GENERAL(99);
    private int value;

    private ErrorCode(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
