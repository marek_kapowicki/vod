package infrastructure.error;

public class ErrorInfo {

    private final int code;
    private final String message;

    ErrorInfo(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
