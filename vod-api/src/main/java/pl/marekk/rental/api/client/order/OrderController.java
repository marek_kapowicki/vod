package pl.marekk.rental.api.client.order;

import ddd.support.query.IdResponse;
import ddd.support.query.PageRepresentation;
import infrastructure.ApiVersions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.marekk.rental.application.OrderManagementService;
import pl.marekk.rental.domain.client.ClientContext;
import pl.marekk.rental.domain.client.order.OrderDto;

import javax.inject.Inject;
import javax.validation.Valid;

@RequestMapping(value = "/orders", consumes = MediaType.APPLICATION_JSON_VALUE)
@RestController
@Slf4j
public class OrderController {

    private final OrderManagementService orderManagementService;
    //TODO fill in base on security
    private final ClientContext clientContext;

    @Inject
    public OrderController(OrderManagementService orderManagementService, ClientContext clientContext) {
        this.orderManagementService = orderManagementService;
        this.clientContext = clientContext;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = ApiVersions.ORDER_JSON_VERSION_1, consumes = ApiVersions.ORDER_JSON_VERSION_1)
    public
    @ResponseBody
    OrderDto findById(@PathVariable("id") String orderId) {
        log.info("delegating finding order by id={} to service", orderId);
        String clientId = clientContext.getClient().getId();
        return orderManagementService.findClientOrderById(clientId, orderId);
    }

    @RequestMapping(method = RequestMethod.GET, produces = ApiVersions.ORDER_JSON_VERSION_1, consumes = ApiVersions.ORDER_JSON_VERSION_1)
    public
    @ResponseBody
    PageRepresentation<OrderDto> findAll(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        log.info("delegating finding orders page={}, limit={} to service", page, limit);
        String clientId = clientContext.getClient().getId();
        return orderManagementService.findClientOrders(clientId, page, limit);
    }

    @RequestMapping(method = RequestMethod.POST, produces = ApiVersions.ORDER_JSON_VERSION_1, consumes = ApiVersions.ORDER_JSON_VERSION_1)
    @ResponseStatus(value = HttpStatus.CREATED)
    public IdResponse create(@RequestBody @Valid CreateOrderRequest createOrderRequest) {
        log.info("delegating create order={} to service", createOrderRequest);
        String currencyCode = createOrderRequest.currency.getCurrencyCode();
        return new IdResponse(orderManagementService.createOrder(clientContext.getClient(), currencyCode,
                createOrderRequest.positions));
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/{orderId}"
            , produces = ApiVersions.ORDER_JSON_VERSION_1, consumes = ApiVersions.ORDER_JSON_VERSION_1)
    public void turnAllMovie(@PathVariable("orderId") String orderId) {
        log.info("delegating turning all items from order={} to service", orderId);
        orderManagementService.turnOrder(clientContext.getClient(), orderId);

    }

    @RequestMapping(method = RequestMethod.PUT,
            value = "/{orderId}/item/{orderItemId}"
            , produces = ApiVersions.ORDER_JSON_VERSION_1, consumes = ApiVersions.ORDER_JSON_VERSION_1)
    public void turnMovie(@PathVariable("orderId") String orderId,
                          @PathVariable("orderItemId") String orderItemId) {
        log.info("delegating turning item={} of order={} to service", orderItemId, orderId);
        orderManagementService.turnOrderItem(clientContext.getClient(), orderId, orderItemId);

    }
}
