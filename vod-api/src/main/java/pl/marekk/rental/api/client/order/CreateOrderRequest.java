package pl.marekk.rental.api.client.order;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import ddd.support.query.BaseRequest;
import pl.marekk.rental.api.CurrencyRepresentation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Map;

public class CreateOrderRequest implements BaseRequest {

    @NotNull(message = "order positions can not be null")
    @Size(min = 1, message = "order positions can not be empty")
    Map<String, Long> positions;

    @NotNull(message = "order currency can not be null")
    CurrencyRepresentation currency;

    @JsonCreator
    public CreateOrderRequest(@JsonProperty("currency") CurrencyRepresentation currency,
                              @JsonProperty("positions") Map<String, Long> positions) {
        this.currency = currency;
        this.positions = positions;
    }
}
