package pl.marekk.rental.api.movie;

import ddd.support.query.IdResponse;
import ddd.support.query.PageRepresentation;
import infrastructure.ApiVersions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.marekk.rental.application.MovieManagementService;
import pl.marekk.rental.domain.movie.MovieDto;

import javax.inject.Inject;
import javax.validation.Valid;

@RequestMapping(value = "/movies", consumes = MediaType.APPLICATION_JSON_VALUE)
@RestController
@Slf4j
public class MovieController {

    private final MovieManagementService movieManagementService;

    @Inject
    public MovieController(MovieManagementService movieManagementService) {
        this.movieManagementService = movieManagementService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = ApiVersions.MOVIE_JSON_VERSION_1, consumes = ApiVersions.MOVIE_JSON_VERSION_1)
    public
    @ResponseBody
    MovieDto findById(@PathVariable("id") String movieId) {
        log.info("delegating finding movie by id={} to service", movieId);
        return movieManagementService.findById(movieId);
    }

    @RequestMapping(method = RequestMethod.GET, produces = ApiVersions.MOVIE_JSON_VERSION_1, consumes = ApiVersions.MOVIE_JSON_VERSION_1)
    public
    @ResponseBody
    PageRepresentation<MovieDto> findAll(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        log.info("delegating finding movies page={}, limit={} to service", page, limit);
        return movieManagementService.findAll(page, limit);
    }

    @RequestMapping(method = RequestMethod.POST, produces = ApiVersions.MOVIE_JSON_VERSION_1, consumes = ApiVersions.MOVIE_JSON_VERSION_1)
    @ResponseStatus(value = HttpStatus.CREATED)
    public IdResponse create(@RequestBody @Valid CreateMovieRequest createMovieRequest) {
        throw new NotImplementedException("admin part of application");
    }


}
