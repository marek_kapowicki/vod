package pl.marekk.rental.api.client;

import ddd.support.query.IdResponse;
import ddd.support.query.PageRepresentation;
import infrastructure.ApiVersions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.marekk.rental.domain.client.ClientDto;
import pl.marekk.rental.domain.movie.MovieDto;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/clients")
@ResponseBody
@Slf4j
public class ClientController {

    @RequestMapping(method = RequestMethod.POST, produces = ApiVersions.CLIENT_JSON_VERSION_1, consumes = ApiVersions.CLIENT_JSON_VERSION_1)
    @ResponseStatus(value = HttpStatus.CREATED)
    public IdResponse registerClient(@RequestBody @Valid RegisterClientRequest createUserRequest) {
        throw new NotImplementedException("not implemented yet");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = ApiVersions.CLIENT_JSON_VERSION_1, consumes = ApiVersions.CLIENT_JSON_VERSION_1)
    public
    @ResponseBody
    ClientDto findById(@PathVariable("id") String clientId) {
        throw new NotImplementedException("not implemented yet");
    }

    @RequestMapping(method = RequestMethod.GET, produces = ApiVersions.CLIENT_JSON_VERSION_1, consumes = ApiVersions.CLIENT_JSON_VERSION_1)
    public
    @ResponseBody
    PageRepresentation<MovieDto> findAll(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        throw new NotImplementedException("not implemented yet");
    }
}
