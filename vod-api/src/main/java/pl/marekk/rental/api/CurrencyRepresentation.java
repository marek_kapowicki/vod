package pl.marekk.rental.api;

import lombok.Getter;

@Getter
public enum CurrencyRepresentation {

    PLN("PLN"), EUR("EUR");

    private final String currencyCode;

    private CurrencyRepresentation(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
