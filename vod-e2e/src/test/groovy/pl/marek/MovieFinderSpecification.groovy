package pl.marek

import configuration.ClientConfiguration
import configuration.MovieRetrofitRestController
import configuration.TestApplication
import org.junit.Ignore
import org.junit.Test
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import pl.marekk.rental.domain.movie.MovieDto
import spock.lang.Specification

import javax.inject.Inject

@ContextConfiguration(loader = SpringApplicationContextLoader.class,
        classes = [TestApplication.class, ClientConfiguration.class])
@WebAppConfiguration
@IntegrationTest
@Ignore
//no data stored in db
//TODO check configuration
class MovieFinderSpecification extends Specification {
    
    @Inject
    private MovieRetrofitRestController movieRetrofitRestController;

    @Test
    def "Find existing movie by id"() {

        given: "Movie identified by id movie_1 is available in store"


        when: "User marek tries to find movie using id"
        MovieDto found = movieRetrofitRestController.findById("movie_1");

        expect: 'found movie has not null id'
        found.id
    }


}