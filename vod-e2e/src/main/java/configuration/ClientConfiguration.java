package configuration;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;


@Configuration
public class ClientConfiguration {

    @Bean
    MovieRetrofitRestController movieRestClient(RestAdapter restAdapter) {
        return restAdapter.create(MovieRetrofitRestController.class);
    }


    @Bean
    RestAdapter roanRestAdapter() {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Content-type", "application/vnd.rental.movie.v1+json");
            }
        };

        return new RestAdapter.Builder()
                .setEndpoint("http://localhost:8080")
                .setRequestInterceptor(requestInterceptor).build();
    }


}
