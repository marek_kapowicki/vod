package configuration;

import pl.marekk.rental.domain.movie.MovieDto;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;

public interface MovieRetrofitRestController {

    @GET("/movies/{id}")
    @Headers(value = "Content-type:application/vnd.rental.movie.v1+json")
    MovieDto findById(@Path("id") String movieId);

}