package pl.marekk.rental.domain.client.order;

import org.joda.money.Money;
import org.junit.Test;

import static pl.marekk.rental.domain.client.ClientAssembler.marek;
import static pl.marekk.rental.domain.client.order.OrderAssembler.DEFAULT_CURRENCY;
import static pl.marekk.rental.domain.client.order.OrderCreateParamsAssembler.emptyParams;
import static pl.marekk.rental.domain.movie.BddAssertions.then;
import static pl.marekk.rental.domain.movie.MovieAssembler.*;

public class InitialPriceFor4ItemsOrderCalculationSpec {

    OrderFactory orderFactory = new OrderFactory(new OrderItemFactory());

    @Test
    public void shouldCalculateProperInitPriceFor4ItemsOrder() throws Exception {

        //given client prepares 4items order
        OrderCreateParams orderCreateParams = emptyParams()
                .currency(DEFAULT_CURRENCY)
                .clientData(marek().assemble().generateSnapshot())
                        //client adds matrix 11 (new movie) for 1 day
                .addOrderItemCreateParams(matrix11().assemble(), 1L)
                        //and adds spider man 2 (regular movie) for  days
                .addOrderItemCreateParams(spiderMan2().assemble(), 2L)
                        //and adds scarface (regular movie) for  5 days
                .addOrderItemCreateParams(spiderMan().assemble(), 5l)
                        //and adds out of africa (old movie) for 7 days
                .addOrderItemCreateParams(outOfAfrica().assemble(), 7L)
                .assemble();


        //when client confirm prepared order
        ClientOrder clientOrder = orderFactory.create(orderCreateParams);


        //then calculated initial price is 250
        OrderDto orderSummary = clientOrder.buildDto();
        then(orderSummary)
                .hasInitialPrice(Money.of(DEFAULT_CURRENCY, 250));
        //and matrix11 costs 40
        then(orderSummary.getItems().get(0)).hasInitialPrice(Money.of(DEFAULT_CURRENCY, 40));
        //and spider-man2 costs 30
        then(orderSummary.getItems().get(1)).hasInitialPrice(Money.of(DEFAULT_CURRENCY, 30));
        //and spider-man costs 90
        then(orderSummary.getItems().get(2)).hasInitialPrice(Money.of(DEFAULT_CURRENCY, 90));

        //and out of africa costs 90
        then(orderSummary.getItems().get(2)).hasInitialPrice(Money.of(DEFAULT_CURRENCY, 90));
    }
}
