package pl.marekk.rental.domain.client;

import org.junit.Test;

import static pl.marekk.rental.domain.client.ClientAssembler.marek;
import static pl.marekk.rental.domain.movie.BddAssertions.then;

public class ClientSnapshotTest {

    @Test
    public void shouldBuildSnapshot() throws Exception {
        //given
        Client client = marek().assemble();
        //when
        ClientData clientData = client.generateSnapshot();
        //then
        then(clientData)
                .hasId(client.getId())
                .hasName(client.getEmail())
                .matches((it) -> it.getSnapshotDate() != null);
    }
}
