package pl.marekk.rental.domain.client.order;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.client.order.OrderItemAssembler.scarfaceItem;

public class TurningItemTest {

    @Test
    public void shouldNotReturnReturnedItem() throws Exception {
        //given
        OrderItem orderItem = scarfaceItem(LocalDate.now(), BigDecimal.ONE).assemble();
        orderItem.turnItem();
        //when
        catchException(orderItem).turnItem();
        //then
        then(caughtException()).isInstanceOf(ItemAlreadyReturnedException.class);
    }

    @Test
    public void shouldSetFinalDateAsTodayDuringTurnItem() throws Exception {
        //given
        OrderItem orderItem = scarfaceItem(LocalDate.now(), BigDecimal.ONE).assemble();
        //when
        orderItem.turnItem();
        //then
        then(orderItem.getFinalReturnDate()).isEqualTo(LocalDate.now());
    }
}
