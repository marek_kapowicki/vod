package pl.marekk.rental.domain.movie;

import org.junit.Test;

import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.movie.MovieAssembler.*;

public class BonusPointsCalculationTest {

    @Test
    public void shouldCalculate1ForOldMovie() throws Exception {
        //given
        Movie oldMovie = scarface().assemble();

        //expect
        then(oldMovie.generateSnapshot().calculateBonusPoints()).isEqualTo(1);
    }

    @Test
    public void shouldCalculate1ForRegularMovie() throws Exception {
        //given
        Movie regularMovie = spiderMan2().assemble();

        //expect
        then(regularMovie.generateSnapshot().calculateBonusPoints()).isEqualTo(1);
    }

    @Test
    public void shouldCalculate2ForNewMovie() throws Exception {
        //given
        Movie regularMovie = matrix11().assemble();

        //expect
        then(regularMovie.generateSnapshot().calculateBonusPoints()).isEqualTo(2);
    }
}
