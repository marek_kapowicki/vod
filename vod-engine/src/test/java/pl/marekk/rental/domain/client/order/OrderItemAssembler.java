package pl.marekk.rental.domain.client.order;

import org.joda.money.Money;
import pl.marekk.rental.domain.movie.Movie;
import pl.marekk.rental.domain.movie.MovieAssembler;
import pl.marekk.rental.domain.movie.MovieData;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

import static org.apache.commons.lang3.RandomStringUtils.random;
import static pl.marekk.rental.domain.client.order.OrderAssembler.DEFAULT_CURRENCY;
import static pl.marekk.rental.domain.movie.MovieAssembler.matrix11;
import static pl.marekk.rental.domain.movie.MovieAssembler.scarface;

public class OrderItemAssembler {

    public static final String SCARFACE_MOVIE_NAME = "scarface";
    protected static final LocalDate START_DATE = LocalDate.of(2015, Month.MAY, 10);

    private String id;
    private MovieData movie;
    private LocalDate initialEndDate;
    private LocalDate finalEndDate;
    private LocalDate startDate;
    private Money initialPrice;
    private Money totalPrice;

    private OrderItemAssembler() {
    }

    public static OrderItemAssembler scarfaceItem(LocalDate initialEndDate, BigDecimal price) {
        return emptyItem(initialEndDate, price)
                .movie(scarface().id(random(4)).assemble().generateSnapshot())
                .id("scarface_item_id");
    }

    public static OrderItemAssembler returnedItem(Movie movie, BigDecimal initPrice, BigDecimal totalPrice) {
        MovieAssembler scarface = scarface();
        return emptyItem(START_DATE, initPrice)
                .movie(movie.generateSnapshot())
                .finalEndDate(START_DATE.plusDays(1))
                .totalPrice(Money.of(DEFAULT_CURRENCY, totalPrice))
                .id(random(3));
    }

    public static OrderItemAssembler matrix11Item(LocalDate initialEndDate, BigDecimal price) {
        return emptyItem(initialEndDate, price)
                .movie(matrix11().id(random(4)).assemble().generateSnapshot())
                .id("matrix11_item_id");
    }

    public static OrderItemAssembler emptyItem() {
        return new OrderItemAssembler();
    }

    public static OrderItemAssembler emptyItem(LocalDate initialEndDate, BigDecimal price) {
        return new OrderItemAssembler()
                .initialPrice(Money.of(DEFAULT_CURRENCY, price))
                .startDate(START_DATE)
                .initialEndDate(initialEndDate);
    }


    public OrderItemAssembler turnItem(LocalDate finalEndDate, BigDecimal totalPrice) {
        return this.finalEndDate(finalEndDate)
                .totalPrice(Money.of(DEFAULT_CURRENCY, totalPrice));
    }

    public OrderItemAssembler addItem(Movie movie) {
        return this
                .movie(movie.generateSnapshot());
    }

    public static OrderItemAssembler anOrderItem() {
        return new OrderItemAssembler();
    }

    public OrderItemAssembler id(String id) {
        this.id = id;
        return this;
    }

    public OrderItemAssembler movie(MovieData movie) {
        this.movie = movie;
        return this;
    }

    public OrderItemAssembler initialEndDate(LocalDate initialEndDate) {
        this.initialEndDate = initialEndDate;
        return this;
    }

    public OrderItemAssembler startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public OrderItemAssembler finalEndDate(LocalDate finalEndDate) {
        this.finalEndDate = finalEndDate;
        return this;
    }

    public OrderItemAssembler initialPrice(Money initialPrice) {
        this.initialPrice = initialPrice;
        return this;
    }

    public OrderItemAssembler totalPrice(Money totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public OrderItemAssembler totalPrice(BigDecimal totalPrice) {
        this.totalPrice = Money.of(DEFAULT_CURRENCY, totalPrice);
        return this;
    }

    public OrderItem assemble() {
        OrderItem orderItem = new OrderItem(movie, startDate, initialEndDate, finalEndDate, initialPrice, totalPrice);
        orderItem.setId(id);
        return orderItem;
    }
}
