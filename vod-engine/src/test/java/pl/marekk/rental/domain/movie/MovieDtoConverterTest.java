package pl.marekk.rental.domain.movie;

import com.google.common.collect.Lists;
import ddd.support.domain.converter.BaseDtoConverter;
import ddd.support.query.PageRepresentation;
import org.junit.Test;
import org.springframework.data.domain.PageImpl;

import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.movie.MovieAssembler.*;


public class MovieDtoConverterTest {

    BaseDtoConverter<Movie, MovieDto> movieDtoConverter = new BaseDtoConverter();

    @Test
    public void shouldConvertFullEntity() throws Exception {
        //given
        String someId = "someId";
        Movie movie = outOfAfrica().id(someId).assemble();

        //when
        MovieDto converted = movieDtoConverter.convert(movie);

        //then
        BddAssertions.then(converted)
                .hasId(converted.getId())
                .hasName(movie.getName())
                .hasDescription(movie.getDescription())
                .hasGrade(movie.getGrade().name())
                .hasCategory(movie.getCategory().name())
                .hasPremiere(movie.getPremiere());
    }

    @Test
    public void shouldConvertEntityWithMandatoryFields() throws Exception {
        //given
        Movie movie = fakeMovie().assemble();
        //when
        MovieDto converted = movieDtoConverter.convert(movie);
        //then
        BddAssertions.then(converted).hasName(movie.getName()).hasPremiere(null);
    }

    @Test
    public void shouldConvertPage() throws Exception {
        //given

        //when
        Movie scarface = scarface().assemble();
        Movie matrix11 = matrix11().assemble();
        PageRepresentation<MovieDto> pageRepresentation = movieDtoConverter.
                convert(new PageImpl(Lists.newArrayList(scarface, matrix11)));
        //then
        then(pageRepresentation.getItems()).extracting("name").contains(scarface.getName(), matrix11.getName());
    }
}
