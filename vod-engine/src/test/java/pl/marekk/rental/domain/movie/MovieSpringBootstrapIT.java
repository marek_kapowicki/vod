package pl.marekk.rental.domain.movie;

import configuration.MovieDomainConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ContextConfiguration(classes = MovieDomainConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class MovieSpringBootstrapIT {

    @Test
    public void shouldStandUp() throws Exception {
        //nothing interesting here
    }
}
