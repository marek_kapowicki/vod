package pl.marekk.rental.domain.client.order;

import org.joda.money.Money;
import org.junit.Test;
import pl.marekk.rental.domain.movie.BddAssertions;

import java.time.LocalDate;

import static pl.marekk.rental.domain.client.ClientAssembler.marek;
import static pl.marekk.rental.domain.client.order.OrderAssembler.*;
import static pl.marekk.rental.domain.client.order.OrderItemAssembler.emptyItem;
import static pl.marekk.rental.domain.movie.MovieAssembler.matrix11;
import static pl.marekk.rental.domain.movie.MovieAssembler.spiderMan2;

public class AdditionalChargeForDelayOrderCalculationSpec {

    @Test
    public void returnDelayOrderWith2Items() throws Exception {
        LocalDate now = LocalDate.now();
        LocalDate promisedMatrix11ReturnDate = now.minusDays(2);
        LocalDate promisedSpiderMan2ReturnDate = now.minusDays(1);
        //given 2 days delay matrix11 and 1 day delay spider-man 2 from the same client order
        ClientOrder delayOrder = emptyOrder()
                .client(marek().assemble().generateSnapshot())
                .addItem(emptyItem()
                        .movie(matrix11().assemble().generateSnapshot())
                        .startDate(ORDER_START_DATE)
                        .initialPrice(Money.zero(DEFAULT_CURRENCY))
                        .initialEndDate(promisedMatrix11ReturnDate).assemble())
                .addItem(emptyItem()
                        .movie(spiderMan2().assemble().generateSnapshot())
                        .initialPrice(Money.zero(DEFAULT_CURRENCY))
                        .startDate(ORDER_START_DATE)
                        .initialEndDate(promisedSpiderMan2ReturnDate).assemble()).assemble();
        //when client returns 2 described items
        delayOrder.getItems().forEach(it -> it.turnItem());

        //then calculated additional charge is equal 110 (80 for matrix, 30 spider-man2)
        OrderDto orderDto = delayOrder.buildDto();
        BddAssertions.then(orderDto)
                .hasAdditionalPrice(Money.of(DEFAULT_CURRENCY, 110));
        BddAssertions.then(orderDto.getItems().get(0))
                .hasAdditionalPrice(Money.of(DEFAULT_CURRENCY, 80));

        BddAssertions.then(orderDto.getItems().get(1))
                .hasAdditionalPrice(Money.of(DEFAULT_CURRENCY, 30));
    }


}
