package pl.marekk.rental.domain.client.order;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.client.order.OrderItemAssembler.emptyItem;
import static pl.marekk.rental.domain.movie.MovieAssembler.*;

public class OrderItemCalculatingBonusPointTest {
    @Test
    public void shouldCalculate1ForItemWithOldMovie() throws Exception {
        //given
        OrderItem oldItem = emptyItem(LocalDate.now(), BigDecimal.ONE)
                .addItem(scarface().assemble()).assemble();

        //then
        then(oldItem.calculateClientBonusPoints()).isEqualTo(1);
    }

    @Test
    public void shouldCalculate1ForItemWithRegularMovie() throws Exception {
        //given
        OrderItem regularItem = emptyItem(LocalDate.now(), BigDecimal.ONE)
                .addItem(spiderMan2().assemble()).assemble();

        //then
        then(regularItem.calculateClientBonusPoints()).isEqualTo(1);
    }

    @Test
    public void shouldCalculate2ForItemWithNewMovie() throws Exception {
        //given
        OrderItem regularItem = emptyItem(LocalDate.now(), BigDecimal.ONE)
                .addItem(matrix11().assemble()).assemble();

        //then
        then(regularItem.calculateClientBonusPoints()).isEqualTo(2);
    }
}
