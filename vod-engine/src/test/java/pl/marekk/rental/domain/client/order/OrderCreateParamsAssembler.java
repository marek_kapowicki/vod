package pl.marekk.rental.domain.client.order;

import com.google.common.collect.Lists;
import org.joda.money.CurrencyUnit;
import pl.marekk.rental.domain.client.ClientData;
import pl.marekk.rental.domain.movie.Movie;

import java.util.List;

import static pl.marekk.rental.domain.client.ClientAssembler.marek;
import static pl.marekk.rental.domain.movie.MovieAssembler.scarface;

public class OrderCreateParamsAssembler {
    private static final CurrencyUnit DEFAULT_CURRENCY = CurrencyUnit.of("PLN");
    private ClientData clientData;
    private CurrencyUnit currency;
    private List<OrderCreateParams.OrderCreatePositionParams> orderItemsCreateParams = Lists.newArrayList();

    private OrderCreateParamsAssembler() {
    }

    public static OrderCreateParamsAssembler wrongParams() {
        return emptyParams()
                .currency(DEFAULT_CURRENCY)
                .clientData(marek().assemble().generateSnapshot())
                .addOrderItemCreateParams(scarface().assemble(), -10);
    }

    public static OrderCreateParamsAssembler validParams(int duration) {
        return emptyParams()
                .currency(DEFAULT_CURRENCY)
                .clientData(marek().assemble().generateSnapshot())
                .addOrderItemCreateParams(scarface().assemble(), duration);
    }

    public static OrderCreateParamsAssembler emptyParams() {
        return new OrderCreateParamsAssembler();
    }

    public OrderCreateParamsAssembler addOrderItemCreateParams(Movie entity, long duration) {
        this.orderItemsCreateParams.add(new OrderCreateParams.OrderCreatePositionParams(entity, duration));
        return this;
    }

    public OrderCreateParamsAssembler clientData(ClientData clientData) {
        this.clientData = clientData;
        return this;
    }

    public OrderCreateParamsAssembler currency(CurrencyUnit currency) {
        this.currency = currency;
        return this;
    }

    public OrderCreateParamsAssembler orderItemsCreateParams(List<OrderCreateParams.OrderCreatePositionParams> orderItemsCreateParams) {
        this.orderItemsCreateParams = orderItemsCreateParams;
        return this;
    }

    public OrderCreateParams assemble() {
        OrderCreateParams orderCreateParams = new OrderCreateParams(clientData, currency, orderItemsCreateParams);
        return orderCreateParams;
    }
}
