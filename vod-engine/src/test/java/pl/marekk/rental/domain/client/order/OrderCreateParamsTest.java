package pl.marekk.rental.domain.client.order;

import org.junit.Test;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.client.order.OrderCreateParamsAssembler.emptyParams;
import static pl.marekk.rental.domain.client.order.OrderCreateParamsAssembler.validParams;
import static pl.marekk.rental.domain.movie.MovieAssembler.matrix11;
import static pl.marekk.rental.domain.movie.MovieAssembler.scarface;

public class OrderCreateParamsTest {

    @Test
    public void emptyParamsAreWrong() throws Exception {
        //when
        catchException(emptyParams().assemble()).checkParams();
        //then
        then(caughtException()).isInstanceOf(OrderCreateException.class);
    }

    @Test
    public void paramsWithoutCurrencyAreWrong() throws Exception {
        //given
        OrderCreateParams params = emptyParams().addOrderItemCreateParams(scarface().assemble(), 6).assemble();

        //when
        catchException(params).checkParams();
        //then
        then(caughtException()).isInstanceOf(OrderCreateException.class);
    }

    @Test
    public void paramsWithoutAnyMovieAreWrong() throws Exception {
        //given
        OrderCreateParams params = emptyParams()
                .addOrderItemCreateParams(scarface().assemble(), 6)
                .addOrderItemCreateParams(null, 7).assemble();

        //when
        catchException(params).checkParams();
        //then
        then(caughtException()).isInstanceOf(OrderCreateException.class);
    }

    @Test
    public void paramsWithoutAnyDurationBelowOAreWrong() throws Exception {
        //given
        OrderCreateParams params = emptyParams()
                .addOrderItemCreateParams(scarface().assemble(), 6)
                .addOrderItemCreateParams(matrix11().assemble(), -5L).assemble();

        //when
        catchException(params).checkParams();
        //then
        then(caughtException()).isInstanceOf(OrderCreateException.class);
    }

    @Test
    public void paramsWithAllMandatoryFieldsAreValid() throws Exception {
        //given
        OrderCreateParams params = validParams(7).assemble();

        //when
        params.checkParams();
        //then no exception was thrown
    }
}
