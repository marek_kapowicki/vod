package pl.marekk.rental.domain.client.order;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

import static pl.marekk.rental.domain.client.order.OrderAssembler.emptyOrder;
import static pl.marekk.rental.domain.client.order.OrderItemAssembler.matrix11Item;
import static pl.marekk.rental.domain.client.order.OrderItemAssembler.scarfaceItem;
import static pl.marekk.rental.domain.movie.BddAssertions.then;

public class OrderDtoDateCalculationTest {

    @Test
    public void shouldNotCalculateEndDateForNotClosedOrder() throws Exception {
        //given
        LocalDate earlierReturnDate = LocalDate.of(2015, Month.FEBRUARY, 10);
        LocalDate latestReturnDate = earlierReturnDate.plusWeeks(2);
        OrderItem earlierItem = scarfaceItem(earlierReturnDate, BigDecimal.ONE).assemble();
        OrderItem latestItem = matrix11Item(latestReturnDate, BigDecimal.ONE).assemble();
        ClientOrder order = emptyOrder()
                .addItem(earlierItem)
                .addItem(latestItem).assemble();
        //when
        OrderDto orderDto = order.buildDto();
        //then
        then(orderDto)
                .hasEndDate(null);
    }

    @Test
    public void shouldCalculateEndDateForClosedOrderAsMaxOfItemRsReturnDate() throws Exception {
        //given
        LocalDate earlierReturnDate = LocalDate.of(2015, Month.FEBRUARY, 10);
        LocalDate latestReturnDate = earlierReturnDate.plusWeeks(2);
        OrderItem earlierItem = scarfaceItem(LocalDate.now(), BigDecimal.ONE)
                .turnItem(earlierReturnDate, BigDecimal.ONE).assemble();
        OrderItem latestItem = matrix11Item(LocalDate.now(), BigDecimal.ONE)
                .turnItem(latestReturnDate, BigDecimal.ONE).assemble();
        ClientOrder order = emptyOrder()
                .addItem(earlierItem)
                .addItem(latestItem).assemble();
        //when
        OrderDto orderDto = order.buildDto();
        //then
        then(orderDto)
                .hasEndDate(latestReturnDate);
    }
}
