package pl.marekk.rental.domain.movie;

import org.junit.Test;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.movie.MovieAssembler.scarface;

public class MovieDataCalculationTest {

    @Test
    public void initPriceFor0DayRentalShouldBeTheSameLikeFor1DayRental() throws Exception {
        //given
        MovieData movieData = scarface().assemble().generateSnapshot();
        //then
        then(movieData.calculateRentalInitPrice(0))
                .isEqualTo(movieData.calculateRentalInitPrice(1));
    }

    @Test
    public void shouldNotCalculateInitPriceForDurationBelow0() throws Exception {
        //given
        MovieData movieData = scarface().assemble().generateSnapshot();

        //when
        catchException(movieData).calculateRentalInitPrice(-5);

        //then
        then(caughtException()).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void shouldNotCalculateAdditionalPriceFor0DayDelay() throws Exception {
        //given
        MovieData movieData = scarface().assemble().generateSnapshot();
        //then
        then(movieData.calculateRentalCharge(0)).isEqualByComparingTo("0");
    }


    @Test
    public void shouldNotCalculateAdditionalPriceForRentalFinishEarlier() throws Exception {
        //given
        MovieData movieData = scarface().assemble().generateSnapshot();
        //then
        then(movieData.calculateRentalCharge(-5)).isEqualByComparingTo("0");
    }

    @Test
    public void shouldCalculatePositiveAdditionalPriceForDeleyMovie() throws Exception {
        //given
        MovieData movieData = scarface().assemble().generateSnapshot();
        //then
        then(movieData.calculateRentalCharge(5)).isNotNegative();
    }
}
