package pl.marekk.rental.domain.client.order;

import configuration.ClientDomainConfiguration;
import configuration.MovieDomainConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.Optional;

import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.client.order.OrderAssembler.emptyOrder;
import static pl.marekk.rental.domain.client.order.OrderAssembler.scarfaceOpenOrder;
import static pl.marekk.rental.domain.client.order.OrderItemAssembler.scarfaceItem;

@ContextConfiguration(classes = {
        ClientDomainConfiguration.class,
        MovieDomainConfiguration.class
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class OrderFindingIT {

    @Inject
    OrderRepository orderRepository;


    @Test
    public void shouldFindClientOrderById() throws Exception {
        //given
        ClientOrder order = scarfaceOpenOrder(10, BigDecimal.valueOf(100.0)).assemble();

        String orderId = orderRepository.save(order).getId();
        //when
        Optional<ClientOrder> found = orderRepository.findByIdAndClientId(orderId, order.getClient().getId());
        //then
        then(found.get().getId()).isEqualTo(orderId);
    }

    @Test
    public void shouldNotFindOrderBelongsToOtherClientById() throws Exception {
        //given

        ClientOrder order = scarfaceOpenOrder(10, BigDecimal.valueOf(100.0)).assemble();
        String orderId = orderRepository.save(order).getId();
        //when
        Optional<ClientOrder> notFoundOrder = orderRepository.findByIdAndClientId("another_client_id", orderId);
        //then
        then(notFoundOrder.isPresent()).isFalse();
    }

    @Test
    public void shouldFindClientOrders() throws Exception {
        //given
        ClientOrder order = scarfaceOpenOrder(10, BigDecimal.valueOf(100.0)).assemble();
        orderRepository.save(order).getId();
        //when
        Page<ClientOrder> clientOrders = orderRepository
                .findByClientIdOrderByStartDateDesc(order.getClient().getId(), new PageRequest(0, 5));
        //then
        then(clientOrders.getContent()).hasSize(1);

    }

    @Test
    public void shouldFind2ClientOrdersOrderedByStartDateDesc() throws Exception {
        //given
        LocalDate newDate = LocalDate.of(2015, Month.APRIL, 5);
        LocalDate oldDate = newDate.minusDays(2);
        ClientOrder secondOrder = scarfaceOpenOrder(10, BigDecimal.valueOf(100.0))
                .startDate(oldDate).assemble();
        ClientOrder firstOrder = emptyOrder()
                .startDate(newDate)
                .addItem(scarfaceItem(LocalDate.now(), BigDecimal.TEN).assemble())
                .assemble();
        orderRepository.save(secondOrder);
        orderRepository.save(firstOrder);
        //when
        Page<ClientOrder> clientOrders = orderRepository
                .findByClientIdOrderByStartDateDesc(secondOrder.getClient().getId(), new PageRequest(0, 5));
        //then
        then(clientOrders.getContent()).hasSize(2);
        ClientOrder firstFound = clientOrders.getContent().get(0);
        ClientOrder secondFound = clientOrders.getContent().get(1);
        then(firstFound.getStartDate()).isAfter(secondFound.getStartDate());
        then(firstFound.getStartDate()).isEqualTo(newDate);

    }
}


