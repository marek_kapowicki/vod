package pl.marekk.rental.domain.client.order;

import org.joda.money.CurrencyUnit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import pl.marekk.rental.domain.movie.Movie;

import java.time.LocalDate;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.movie.MovieAssembler.scarface;

@RunWith(MockitoJUnitRunner.class)
public class OrderItemFactoryTest {

    @InjectMocks
    OrderItemFactory orderItemFactory;

    @Test
    public void shouldNotCreateItemFromEmptyMovie() throws Exception {
        //when
        catchException(orderItemFactory).create(null, CurrencyUnit.EUR, 10);
        //then
        then(caughtException()).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void shouldNotCreateItemFromEmptyCurrency() throws Exception {
        //when
        catchException(orderItemFactory).create(scarface().assemble(), null, 10);
        //then
        then(caughtException()).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void shouldNotCreateItemFromWrongDuration() throws Exception {
        //when
        catchException(orderItemFactory).create(scarface().assemble(), CurrencyUnit.EUR, -1);
        //then
        then(caughtException()).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void shouldCreateOpenItem() throws Exception {
        //when
        Movie itemMovie = scarface().id("moveId").assemble();
        int durationInDays = 1;
        CurrencyUnit currency = CurrencyUnit.EUR;
        OrderItem item = orderItemFactory.create(itemMovie, currency, durationInDays);
        //then
        then(item.getMovie().getName()).isEqualTo(itemMovie.generateSnapshot().getName());
        then(item.getMovie().getId()).isEqualTo(itemMovie.getId());
        then(item.getInitialPrice().getAmount()).isNotNegative();
        then(item.getInitialPrice().getCurrencyUnit()).isEqualTo(currency);
        then(item.getInitialReturnDate()).isEqualTo(LocalDate.now().plusDays(durationInDays));

    }
}
