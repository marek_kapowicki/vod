package pl.marekk.rental.domain.movie;

import configuration.MovieDomainConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Optional;

import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.movie.MovieAssembler.matrix11;

@ContextConfiguration(classes = MovieDomainConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class MovieFindingIT {


    @Inject
    MovieRepository movieRepository;


    @Test
    public void shouldFindEmptyCollection() throws Exception {
        //when
        Page<Movie> found = movieRepository.findAll(new PageRequest(0, 5));
        //then
        then(found.getTotalElements()).isZero();
    }

    @Test
    public void shouldFindMovieById() throws Exception {
        //given
        Movie movie = matrix11().assemble();
        String id = movieRepository.save(movie).getId();
        //when
        Movie found = movieRepository.findById(id).get();
        //then
        then(found.getId()).isEqualTo(id);
    }

    @Test
    public void shouldFindMoviePage() throws Exception {
        //given
        Movie movie = matrix11().assemble();
        movieRepository.save(movie);
        //when
        Page<Movie> entityPage = movieRepository.findAll(new PageRequest(0, 2));
        //then
        then(entityPage.getTotalElements()).isEqualTo(1);
    }

    @Test
    public void shouldFillAuditData() throws Exception {
        //given
        Movie movie = matrix11().assemble();
        String id = movieRepository.save(movie).getId();
        //when
        Optional<Movie> found = movieRepository.findById(id);
        then(found.get().creationDate()).isNotNull();
        then(found.get().lastModifiedDate()).isNotNull();
    }

}
