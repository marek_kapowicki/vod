package pl.marekk.rental.domain.movie;

import java.math.BigDecimal;

public class MovieCalculationConstants {
    public static final BigDecimal TEST_BASIC_PRICE = BigDecimal.valueOf(30);
    public static final BigDecimal TEST_PREMIUM_PRICE = BigDecimal.valueOf(40);
    public static final int TEST_OLD_POLICY_PERIOD_DURATION = 5;
    public static final int TEST_REGULAR_POLICY_PERIOD_DURATION = 3;
    public static final int TEST_NEW_POLICY_PERIOD_DURATION = 1;
}
