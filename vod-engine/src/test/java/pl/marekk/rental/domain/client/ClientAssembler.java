package pl.marekk.rental.domain.client;

public class ClientAssembler {
    private String id;
    private String email;
    private String password;

    private ClientAssembler() {
    }

    public static ClientAssembler marek() {
        ClientAssembler assembler = new ClientAssembler();
        return assembler.id("client_1").email("marekk@gmail.com").password("password");
    }

    public ClientAssembler id(String id) {
        this.id = id;
        return this;
    }

    public ClientAssembler email(String email) {
        this.email = email;
        return this;
    }

    public ClientAssembler password(String password) {
        this.password = password;
        return this;
    }


    public Client assemble() {
        Client client = new Client(email, password, 0);
        client.setId(id);
        return client;
    }
}
