package pl.marekk.rental.domain.client.order;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.RandomStringUtils;
import org.joda.money.CurrencyUnit;
import pl.marekk.rental.domain.client.ClientData;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static pl.marekk.rental.domain.client.ClientAssembler.marek;
import static pl.marekk.rental.domain.client.order.OrderItemAssembler.scarfaceItem;

public class OrderAssembler {
    private static final DecimalFormat MONEY_FORMAT = new DecimalFormat("#0.00##");
    public static final LocalDate ORDER_START_DATE = LocalDate.of(2015, Month.MARCH, 10);
    public static final CurrencyUnit DEFAULT_CURRENCY = CurrencyUnit.of("PLN");
    private String id;
    private List<OrderItem> items = Lists.newArrayList();
    private ClientData client;
    private LocalDate startDate;


    private OrderAssembler() {
    }

    public static OrderAssembler closedOrder(int durationInDays, int finalDurationInDays,
                                             BigDecimal initialPrice, BigDecimal totalPrice) {
        return emptyOrder()
                .addItem(scarfaceItem(ORDER_START_DATE.plusDays(durationInDays), initialPrice)
                        .turnItem(ORDER_START_DATE.plusDays(finalDurationInDays), totalPrice).assemble());

    }

    public static OrderAssembler scarfaceOpenOrder(int durationInDays, BigDecimal price) {
        return emptyOrder()
                .addItem(scarfaceItem(ORDER_START_DATE.plusDays(durationInDays), price)
                        .id(RandomStringUtils.random(3)).assemble());
    }

    public static OrderAssembler emptyOrder() {
        OrderAssembler assembler = new OrderAssembler();
        return assembler
                .client(marek().assemble().generateSnapshot())
                .startDate(ORDER_START_DATE);
    }

    public OrderAssembler addItem(OrderItem item) {
        this.items.add(item);
        return this;
    }

    public static OrderAssembler anOrder() {
        return new OrderAssembler();
    }


    public OrderAssembler id(String id) {
        this.id = id;
        return this;
    }

    public OrderAssembler items(List<OrderItem> items) {
        this.items = items;
        return this;
    }

    public OrderAssembler client(ClientData client) {
        this.client = client;
        return this;
    }

    public OrderAssembler startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public ClientOrder assemble() {
        ClientOrder order = new ClientOrder(items, client, startDate);
        order.setId(id);
        return order;
    }

    public static String formatMoney(Number money) {
        return MONEY_FORMAT.format(money);
    }
}
