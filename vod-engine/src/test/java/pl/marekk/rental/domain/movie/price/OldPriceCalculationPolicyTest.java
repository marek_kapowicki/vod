package pl.marekk.rental.domain.movie.price;

import org.junit.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.movie.price.AbstractPriceCalculationPolicy.BASIC_PRICE;
import static pl.marekk.rental.domain.movie.price.OldPriceCalculationPolicy.OLD_POLICY_PERIOD_DURATION;

public class OldPriceCalculationPolicyTest {

    OldPriceCalculationPolicy calculationPolicy = new OldPriceCalculationPolicy();

    @Test
    public void shouldCalculateBasePriceForLoanShorterThenUnitDuration() throws Exception {

        //when
        BigDecimal calculatedPrice = calculationPolicy.calculate(3);
        //then
        then(calculatedPrice).isEqualTo(BASIC_PRICE);
    }

    @Test
    public void shouldCalculatePriceAsBasePriceForLoanEqThenUnitDuration() throws Exception {

        //when
        BigDecimal calculatedPrice = calculationPolicy.calculate(OLD_POLICY_PERIOD_DURATION);
        //then
        then(calculatedPrice).isEqualByComparingTo(BASIC_PRICE);
    }

    @Test
    public void shouldCalculatePriceAsBasePricePlusFeeForLoanLongerThenUnitDuration() throws Exception {

        //when
        BigDecimal calculatedPrice = calculationPolicy.calculate(OLD_POLICY_PERIOD_DURATION + 2);
        //then
        BigDecimal feeAmount = BASIC_PRICE.multiply(BigDecimal.valueOf(2.0));
        then(calculatedPrice).isEqualByComparingTo(BASIC_PRICE.add(feeAmount));
    }


    @Test
    public void shouldCalculateChargeAs2xRenalPriceFor2DaysLoan() throws Exception {
        //when
        BigDecimal calculatedPrice = calculationPolicy.calculateCharge(2);
        //then
        then(calculatedPrice).isEqualByComparingTo(BASIC_PRICE.multiply(valueOf(2)));
    }
}
