package pl.marekk.rental.domain.movie;

import org.junit.Test;

import static pl.marekk.rental.domain.movie.BddAssertions.then;
import static pl.marekk.rental.domain.movie.MovieAssembler.matrix11;

public class MovieSnapshotTest {

    @Test
    public void shouldBuildSnapshot() throws Exception {
        //given
        Movie entity = matrix11().id("someId").assemble();
        //when
        MovieData movieData = entity.generateSnapshot();
        //then
        then(movieData)
                .hasName(entity.getName())
                .hasId(entity.getId())
                .matches((it) -> it.getSnapshotDate() != null);
    }
}
