package pl.marekk.rental.domain.movie.price;

import org.junit.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.movie.price.NewReleasePriceCalculationPolicy.PREMIUM_PRICE;

public class NewReleasePriceCalculationPolicyTest {


    NewReleasePriceCalculationPolicy calculationPolicy = new NewReleasePriceCalculationPolicy();

    @Test
    public void shouldCalculatePriceAs1xRentalPriceFor1DayLoan() throws Exception {
        //when
        BigDecimal calculatedPrice = calculationPolicy.calculate(1);
        //then
        then(calculatedPrice).isEqualByComparingTo(PREMIUM_PRICE);
    }

    @Test
    public void shouldCalculatePriceAs5xPremiumPriceFor5DaysLoan() throws Exception {
        //when
        BigDecimal calculatedPrice = calculationPolicy.calculate(5);
        //then
        then(calculatedPrice).isEqualByComparingTo(PREMIUM_PRICE.multiply(valueOf(5)));
    }

    @Test
    public void shouldCalculateChargeAs2xRenalPriceFor2DaysLoan() throws Exception {
        //when
        BigDecimal calculatedPrice = calculationPolicy.calculateCharge(2);
        //then
        then(calculatedPrice).isEqualByComparingTo(PREMIUM_PRICE.multiply(valueOf(2)));
    }
}
