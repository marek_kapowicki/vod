package pl.marekk.rental.domain.client;

import com.google.common.eventbus.EventBus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;
import static pl.marekk.rental.domain.client.ClientAssembler.marek;

@RunWith(MockitoJUnitRunner.class)
@Transactional
public class ClientBonusPointsListenerTest {

    @InjectMocks
    ClientBonusPointsListener listener;

    @Mock
    ClientRepository clientRepository;


    @Test
    public void shouldAddBonusPointsToClient() throws Exception {
        //given
        EventBus eventBus = new EventBus("test");
        String clientId = "someClient";
        int bonusPoints = 10;
        Client client = marek().assemble();
        given(clientRepository.findOne(clientId)).willReturn(client);

        eventBus.register(listener);
        //when

        AddBonusPointsEvent event = new AddBonusPointsEvent(clientId, bonusPoints);
        eventBus.post(event);
        //then
        then(client.getBonusPoints()).isEqualTo(bonusPoints);

    }
}
