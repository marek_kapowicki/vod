package pl.marekk.rental.domain.client.order;

import org.joda.money.CurrencyUnit;
import org.junit.Test;

import java.time.LocalDate;

import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.movie.MovieAssembler.scarface;

public class OrderItemCalculatingPriceTest {

    OrderItemFactory orderItemFactory = new OrderItemFactory();

    @Test
    public void shouldCalculateInitPriceForODayRental() throws Exception {
        //given
        OrderItem item = orderItemFactory.create(scarface().assemble(), CurrencyUnit.EUR, 0);

        //then
        then(item.getInitialReturnDate()).isEqualTo(LocalDate.now());
    }
}
