package pl.marekk.rental.domain.client.order;

import org.joda.money.CurrencyUnit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.marekk.rental.domain.movie.Movie;

import java.time.LocalDate;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static pl.marekk.rental.domain.client.order.OrderCreateParamsAssembler.validParams;
import static pl.marekk.rental.domain.client.order.OrderCreateParamsAssembler.wrongParams;

@RunWith(MockitoJUnitRunner.class)
@Transactional
public class OrderFactoryTest {
    @InjectMocks
    OrderFactory orderFactory;

    @Mock
    OrderItemFactory orderItemFactory;

    @Mock
    OrderItem orderItem;

    @Before
    public void setUp() throws Exception {
        given(orderItemFactory.create(any(Movie.class), any(CurrencyUnit.class), anyLong()))
                .willReturn(orderItem);

    }

    @Test
    public void shouldBuildOrderBaseOnProperRequest() throws Exception {
        //given

        //when
        ClientOrder order = orderFactory.create(validParams(5).assemble());
        //then
        then(order.getClient().getId()).isNotEmpty();
        then(order.getStartDate()).isEqualTo(LocalDate.now());
    }

    @Test
    public void shouldNotBuildOrderBaseOnWrongRequest() throws Exception {
        //given

        //when
        catchException(orderFactory).create(wrongParams().assemble());
        //then
        then(caughtException()).isInstanceOf(OrderCreateException.class);

    }
}
