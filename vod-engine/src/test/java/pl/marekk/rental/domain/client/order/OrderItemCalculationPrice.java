package pl.marekk.rental.domain.client.order;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import pl.marekk.rental.domain.movie.Movie;
import pl.marekk.rental.domain.movie.MovieCalculationConstants;

import java.math.BigDecimal;

import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.movie.MovieAssembler.*;
import static pl.marekk.rental.domain.movie.MovieCalculationConstants.TEST_BASIC_PRICE;
import static pl.marekk.rental.domain.movie.MovieCalculationConstants.TEST_PREMIUM_PRICE;


@RunWith(MockitoJUnitRunner.class)
public class OrderItemCalculationPrice {

    @InjectMocks
    OrderItemFactory orderItemFactory;

    @Test
    public void shouldCalculateBasicPriceDuringCreationItemWithOldMovie() throws Exception {
        //given
        Movie oldMovie = scarface().id("moveId").assemble();
        int durationInDays = MovieCalculationConstants.TEST_OLD_POLICY_PERIOD_DURATION;
        CurrencyUnit currency = CurrencyUnit.EUR;
        OrderItem item = orderItemFactory.create(oldMovie, currency, durationInDays);
        //then
        then(item.getInitialPrice()).isEqualTo(Money.of(currency, TEST_BASIC_PRICE));
    }

    @Test
    public void shouldCalculate2xBasicPriceDuringCreation1DayDelayedItemWithOldMovie() throws Exception {
        //given
        Movie oldMovie = scarface().id("moveId").assemble();
        int durationInDays = MovieCalculationConstants.TEST_OLD_POLICY_PERIOD_DURATION + 1;
        CurrencyUnit currency = CurrencyUnit.EUR;
        OrderItem item = orderItemFactory.create(oldMovie, currency, durationInDays);
        //then
        then(item.getInitialPrice()).isEqualTo(Money.of(currency, TEST_BASIC_PRICE.add(TEST_BASIC_PRICE)));
    }

    @Test
    public void shouldCalculateBasicPriceDuringCreationItemWithRegularMovie() throws Exception {
        //given
        Movie regularMovie = spiderMan2().id("moveId").assemble();
        int durationInDays = MovieCalculationConstants.TEST_REGULAR_POLICY_PERIOD_DURATION;
        CurrencyUnit currency = CurrencyUnit.EUR;
        OrderItem item = orderItemFactory.create(regularMovie, currency, durationInDays);
        //then
        then(item.getInitialPrice()).isEqualTo(Money.of(currency, TEST_BASIC_PRICE));
    }

    @Test
    public void shouldCalculate2xBasicPriceDuringCreation1DayDelayedItemWithRegularMovie() throws Exception {
        //given
        Movie regularMovie = spiderMan2().id("moveId").assemble();
        int durationInDays = MovieCalculationConstants.TEST_REGULAR_POLICY_PERIOD_DURATION + 1;
        CurrencyUnit currency = CurrencyUnit.EUR;
        OrderItem item = orderItemFactory.create(regularMovie, currency, durationInDays);
        //then
        then(item.getInitialPrice()).isEqualTo(Money.of(currency, TEST_BASIC_PRICE.add(TEST_BASIC_PRICE)));
    }

    @Test
    public void shouldCalculatePremiumPriceDurationForItemWithNewMovie() throws Exception {
        //given
        Movie newMovie = matrix11().id("moveId").assemble();
        int durationInDays = 3;
        CurrencyUnit currency = CurrencyUnit.EUR;
        OrderItem item = orderItemFactory.create(newMovie, currency, durationInDays);
        BigDecimal expectedPrice = TEST_PREMIUM_PRICE.multiply(BigDecimal.valueOf(durationInDays));
        then(item.getInitialPrice()).isEqualTo(Money.of(currency, expectedPrice));
    }
}
