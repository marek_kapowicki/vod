package pl.marekk.rental.domain.client.order;

import org.joda.money.Money;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

import static pl.marekk.rental.domain.client.order.OrderAssembler.DEFAULT_CURRENCY;
import static pl.marekk.rental.domain.client.order.OrderItemAssembler.START_DATE;
import static pl.marekk.rental.domain.client.order.OrderItemAssembler.scarfaceItem;
import static pl.marekk.rental.domain.movie.BddAssertions.then;

public class OrderItemDtoBuildingTest {


    @Test
    public void shouldSetAdditionalCost0ForNotReturnedItem() throws Exception {
        //given
        OrderItem item = scarfaceItem(START_DATE.plusDays(2), BigDecimal.ONE).assemble();
        //when
        OrderItemDto orderItemDto = item.buildDto();
        //then
        then(orderItemDto).hasAdditionalPrice(Money.zero(DEFAULT_CURRENCY));
    }

    @Test
    public void shouldSetAdditionalCostForReturnedItem() throws Exception {
        //given
        BigDecimal initPrice = BigDecimal.valueOf(5);
        BigDecimal totalPrice = BigDecimal.valueOf(10);
        LocalDate finalReturnDate = LocalDate.of(2010, Month.MAY, 6);

        OrderItem item = scarfaceItem(START_DATE, initPrice)
                .totalPrice(Money.of(DEFAULT_CURRENCY, totalPrice))
                .finalEndDate(finalReturnDate)
                .assemble();
        //when
        OrderItemDto orderItemDto = item.buildDto();
        //then
        then(orderItemDto)
                .hasFinalReturnDate(finalReturnDate)
                .hasAdditionalPrice(Money.of(DEFAULT_CURRENCY, totalPrice.subtract(initPrice)));
    }

    @Test
    public void shouldBuildDtoFromOpenItem() throws Exception {
        //given
        LocalDate initialEndDate = OrderAssembler.ORDER_START_DATE.plusDays(10);
        BigDecimal initialPrice = BigDecimal.valueOf(30);
        OrderItem item = scarfaceItem(initialEndDate, initialPrice).assemble();
        //when
        OrderItemDto orderItemDto = item.buildDto();
        //then
        then(orderItemDto)
                .hasId(item.getId())
                .hasMovieName(item.getMovie().getName())
                .hasInitialPrice(Money.of(DEFAULT_CURRENCY, initialPrice))
                .hasInitialReturnDate(initialEndDate)
                .hasFinalReturnDate(null);
    }


}
