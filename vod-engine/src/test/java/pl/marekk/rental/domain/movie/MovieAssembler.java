package pl.marekk.rental.domain.movie;

import java.time.LocalDate;
import java.time.Month;

import static pl.marekk.rental.domain.movie.MovieCategory.*;
import static pl.marekk.rental.domain.movie.MovieGrade.*;

public class MovieAssembler {
    private String id;
    private String name;
    private MovieCategory category;
    private LocalDate premiere;
    private String description;
    private MovieGrade grade;

    private MovieAssembler() {
    }

    public static MovieAssembler matrix11() {
        return new MovieAssembler()
                .id("matrix_id")
                .name("matrix11")
                .description("super turbo cool")
                .grade(ACTION)
                .category(NEW_RELEASE)
                .premiere(LocalDate.of(2015, Month.MAY, 8));
    }

    public static MovieAssembler spiderMan2() {
        return new MovieAssembler()
                .name("spider-man2")
                .description("super turbo cool")
                .grade(ACTION)
                .category(REGULAR)
                .id("spider_man_id")
                .premiere(LocalDate.of(2014, Month.APRIL, 21));
    }

    public static MovieAssembler spiderMan() {
        return new MovieAssembler()
                .name("spider-man")
                .description("old and not so fun")
                .grade(ACTION)
                .category(REGULAR)
                .id("old_spider_man")
                .premiere(LocalDate.of(2005, Month.APRIL, 21));
    }


    public static MovieAssembler scarface() {
        return new MovieAssembler()
                .name("scarface")
                .id("some_id")
                .description("word of Tony Montana")
                .grade(CRIME)
                .category(OLD)
                .premiere(LocalDate.of(1983, Month.DECEMBER, 8));

    }

    public static MovieAssembler outOfAfrica() {
        return new MovieAssembler()
                .name("out of africa")
                .id("out_of_africa_id")
                .description("somewhere in Africa")
                .grade(FAMILY)
                .category(OLD)
                .premiere(LocalDate.of(1985, Month.DECEMBER, 20));

    }

    public static MovieAssembler fakeMovie() {
        MovieAssembler assembler = new MovieAssembler();
        assembler
                .name("fake")
                .category(MovieCategory.NEW_RELEASE)
                .description("for some tests");
        return assembler;
    }


    public MovieAssembler id(String id) {
        this.id = id;
        return this;
    }

    public MovieAssembler name(String name) {
        this.name = name;
        return this;
    }

    public MovieAssembler category(MovieCategory category) {
        this.category = category;
        return this;
    }

    public MovieAssembler premiere(LocalDate premiere) {
        this.premiere = premiere;
        return this;
    }

    public MovieAssembler description(String description) {
        this.description = description;
        return this;
    }

    public MovieAssembler grade(MovieGrade grade) {
        this.grade = grade;
        return this;
    }

    public Movie assemble() {
        Movie movie = new Movie(name, category, premiere, description, grade);
        movie.setId(id);
        return movie;
    }
}
