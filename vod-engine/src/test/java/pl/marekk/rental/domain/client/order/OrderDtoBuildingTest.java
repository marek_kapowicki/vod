package pl.marekk.rental.domain.client.order;

import org.joda.money.Money;
import org.junit.Test;
import pl.marekk.rental.domain.movie.BddAssertions;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.client.order.OrderAssembler.*;
import static pl.marekk.rental.domain.client.order.OrderItemAssembler.*;
import static pl.marekk.rental.domain.movie.MovieAssembler.outOfAfrica;
import static pl.marekk.rental.domain.movie.MovieAssembler.scarface;

public class OrderDtoBuildingTest {

    @Test
    public void shouldCalculateInitPriceAsSumOfItemsInitPrice() throws Exception {
        //given
        BigDecimal item1InitPrice = BigDecimal.valueOf(10);
        BigDecimal item2InitPrice = BigDecimal.valueOf(60);
        ClientOrder order = emptyOrder()
                .addItem(scarfaceItem(LocalDate.now(), item1InitPrice).assemble())
                .addItem(emptyItem(LocalDate.now(), item2InitPrice)
                        .addItem(outOfAfrica().id("movie1").assemble()).assemble())
                .assemble();
        //when
        OrderDto orderDto = order.buildDto();
        //then
        BddAssertions.then(orderDto).hasInitialPrice(Money.of(DEFAULT_CURRENCY, item1InitPrice.add(item2InitPrice)));
    }

    @Test
    public void shouldNotCalculateEndDateForOpenOrder() throws Exception {
        //given

        ClientOrder order = emptyOrder()
                .addItem(scarfaceItem(LocalDate.now(), BigDecimal.ONE)
                        .turnItem(LocalDate.now(), BigDecimal.ONE).assemble())
                .addItem(emptyItem(LocalDate.now(), BigDecimal.ONE)
                        .addItem(outOfAfrica().id("movie1").assemble()).assemble())
                .assemble();
        //when
        OrderDto orderDto = order.buildDto();
        //then
        BddAssertions.then(orderDto).hasEndDate(null);
    }

    @Test
    public void shouldNotCalculateAdditionalPriceForOrderWithAllNotReturnedItems() throws Exception {
        //given

        ClientOrder order = emptyOrder()
                .addItem(scarfaceItem(LocalDate.now(), BigDecimal.ONE).assemble())
                .addItem(emptyItem(LocalDate.now(), BigDecimal.ONE)
                        .addItem(outOfAfrica().id("movie1").assemble()).assemble())
                .assemble();
        //when
        OrderDto orderDto = order.buildDto();
        //then
        BddAssertions.then(orderDto).hasAdditionalPrice(Money.zero(DEFAULT_CURRENCY));
    }

    @Test
    public void shouldNotCalculateAdditionalPriceOnlyFromReturnedItems() throws Exception {
        //given

        BigDecimal returnedItemTotalPrice = BigDecimal.valueOf(10);
        BigDecimal returnedItemInitPrice = BigDecimal.ONE;
        OrderItem returnedItem = returnedItem(scarface().assemble(), returnedItemInitPrice, returnedItemTotalPrice).assemble();
        ClientOrder order = emptyOrder()
                .addItem(returnedItem)

                .addItem(emptyItem(LocalDate.now(), BigDecimal.ONE)
                        .addItem(outOfAfrica().id("movie1").assemble()).assemble())
                .assemble();
        //when
        OrderDto orderDto = order.buildDto();
        //then
        BddAssertions.then(orderDto).hasAdditionalPrice(Money.of(DEFAULT_CURRENCY, returnedItemTotalPrice.subtract(returnedItemInitPrice)));
    }

    @Test
    public void shouldCalculateClosedOrderAdditionalPriceAsSumOfAllItems() throws Exception {
        //given
        BigDecimal initPrice1 = BigDecimal.valueOf(20);
        BigDecimal finalPrice1 = BigDecimal.valueOf(50);
        BigDecimal initPrice2 = BigDecimal.valueOf(15);
        BigDecimal finalPrice2 = BigDecimal.valueOf(20);
        ClientOrder order = emptyOrder()
                .addItem(returnedItem(scarface().assemble(), initPrice1, finalPrice1).assemble())
                .addItem(returnedItem(outOfAfrica().assemble(), initPrice2, finalPrice2).assemble())
                .assemble();
        //when
        OrderDto orderDto = order.buildDto();
        //then
        BddAssertions.then(orderDto).hasAdditionalPrice(Money.of(DEFAULT_CURRENCY, BigDecimal.valueOf(35)));
    }

    @Test
    public void shouldBuildFinishDateOfClosedOrderAsMaxOfReturnItemsDate() throws Exception {
        //given
        LocalDate earlierDate = LocalDate.of(2010, Month.MAY, 5);
        LocalDate newestDate = earlierDate.plusDays(3);
        ClientOrder order = emptyOrder()
                .addItem(returnedItem(scarface().assemble(), BigDecimal.ONE, BigDecimal.ONE)
                        .finalEndDate(earlierDate).assemble())
                .addItem(returnedItem(outOfAfrica().assemble(), BigDecimal.ONE, BigDecimal.ONE)
                        .finalEndDate(newestDate).assemble())
                .assemble();
        //when
        OrderDto orderDto = order.buildDto();
        //then
        BddAssertions.then(orderDto).hasEndDate(newestDate);
    }

    @Test
    public void shouldBuildDtoFromInProgressOrder() throws Exception {
        //given
        int duration = 7;
        BigDecimal cost = BigDecimal.TEN;
        ClientOrder order = scarfaceOpenOrder(duration, cost).assemble();
        //when
        OrderDto orderDto = order.buildDto();
        //then
        BddAssertions.then(orderDto)
                .hasStartDate(ORDER_START_DATE)
                .hasOrderStatus(OrderStatus.IN_PROGRESS.name());
        then(orderDto.getItems()).extracting("movieName").contains(OrderItemAssembler.SCARFACE_MOVIE_NAME);
    }

    @Test
    public void shouldBuildDtoFromClosedOrder() throws Exception {
        //given
        String orderId = "order_1";
        int duration = 7;
        int realDuration = 10;
        BigDecimal cost = BigDecimal.TEN;
        BigDecimal realCost = BigDecimal.valueOf(17.0);
        ClientOrder order = closedOrder(duration, realDuration, cost, realCost).id(orderId).assemble();
        //when
        OrderDto orderDto = order.buildDto();
        //then
        BddAssertions.then(orderDto)
                .hasStartDate(ORDER_START_DATE)
                .hasOrderStatus(OrderStatus.CLOSED.name())
                .hasId(orderId);
    }

    @Test
    public void shouldBuildDtoFrom2ItemsOrder() throws Exception {
        //given
        String movieId1 = "movie1";
        String movieId2 = "movie2";
        ClientOrder order = emptyOrder()
                .addItem(scarfaceItem(LocalDate.now(), BigDecimal.valueOf(10))
                        .id(movieId1).assemble())
                .addItem(emptyItem(LocalDate.now(), BigDecimal.valueOf(60))
                        .addItem(outOfAfrica().id("movie1").assemble())
                        .id(movieId2).assemble())
                .assemble();
        //when
        OrderDto orderDto = order.buildDto();
        //then
        then(orderDto.getItems()).hasSize(2).extracting("id").contains(movieId1, movieId2);
    }
}
