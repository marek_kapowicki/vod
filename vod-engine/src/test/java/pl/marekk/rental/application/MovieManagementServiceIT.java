package pl.marekk.rental.application;

import com.google.common.collect.Lists;
import configuration.MainDomainConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.marekk.rental.domain.movie.Movie;
import pl.marekk.rental.domain.movie.MovieRepository;
import pl.marekk.rental.domain.movie.RequestedMovieDoesNotExists;

import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.movie.MovieAssembler.outOfAfrica;
import static pl.marekk.rental.domain.movie.MovieAssembler.scarface;

@ContextConfiguration(classes = MainDomainConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class MovieManagementServiceIT {

    private String MOVIE_ID;
    @Inject
    MovieRepository movieRepository;

    @Inject
    MovieManagementService movieManagementService;

    @Before
    public void setUp() throws Exception {
        MOVIE_ID = movieRepository.save(scarface().assemble()).getId();
    }

    @Test
    public void shouldTrowExceptionWhenTooFewMoviesWasFound() throws Exception {

        //expect
        assertThatThrownBy(() -> {
            movieManagementService.findAllMoviesFromList(Lists.newArrayList(MOVIE_ID, "not_existing id"));
        }).isInstanceOf(RequestedMovieDoesNotExists.class).hasMessageContaining("not_existing id");

    }

    @Test
    public void shouldTrowExceptionForNullIds() throws Exception {

        assertThatThrownBy(() -> {
            movieManagementService.findAllMoviesFromList(Lists.newArrayList());
        }).isInstanceOf(IllegalArgumentException.class);

    }

    @Test
    public void shouldFoundMoviesByIds() throws Exception {
        //given
        String id2 = movieRepository.save(outOfAfrica().assemble()).getId();
        //when
        List<Movie> found = movieManagementService.findAllMoviesFromList(Lists.newArrayList(MOVIE_ID, id2));
        //then
        then(found).hasSize(2);
    }
}
