package pl.marekk.rental.application;

import com.google.common.collect.ImmutableMap;
import configuration.MainDomainConfiguration;
import org.assertj.core.api.BDDAssertions;
import org.joda.money.IllegalCurrencyException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.marekk.rental.domain.client.Client;
import pl.marekk.rental.domain.client.ClientData;
import pl.marekk.rental.domain.client.ClientRepository;
import pl.marekk.rental.domain.client.order.OrderItemDto;
import pl.marekk.rental.domain.client.order.OrderItemTurnException;
import pl.marekk.rental.domain.client.order.OrderRepository;
import pl.marekk.rental.domain.movie.MovieRepository;
import pl.marekk.rental.domain.movie.RequestedMovieDoesNotExists;

import javax.inject.Inject;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.BDDAssertions.then;
import static pl.marekk.rental.domain.client.ClientAssembler.marek;
import static pl.marekk.rental.domain.movie.MovieAssembler.scarface;

@ContextConfiguration(classes = MainDomainConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class OrderManagementServiceIT {

    private String MOVIE_ID;
    @Inject
    MovieRepository movieRepository;

    @Inject
    OrderManagementService orderManagementService;


    @Inject
    ClientRepository clientRepository;

    @Inject
    OrderRepository orderRepository;


    @Before
    public void setUp() throws Exception {
        MOVIE_ID = movieRepository.save(scarface().assemble()).getId();
    }

    @Test
    public void shouldThrowExceptionDuringCreationOrderFromNotExistingMovie() throws Exception {
        //given
        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put("not_existing_id", 1L).build();
        ClientData clientData = marek().assemble().generateSnapshot();

        //expect
        assertThatThrownBy(() -> {
            orderManagementService.createOrder(clientData, "PLN", positions);
        }).isInstanceOf(RequestedMovieDoesNotExists.class).hasMessageContaining("not_existing_id");
    }

    @Test
    public void shouldThrowExceptionDuringCreationOrderWithWrongCurrency() throws Exception {
        //given
        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put(MOVIE_ID, 1L)
                .build();
        ClientData clientData = marek().assemble().generateSnapshot();
        //expect
        assertThatThrownBy(() -> {
            orderManagementService.createOrder(clientData, "WRONG", positions);
        }).isInstanceOf(IllegalCurrencyException.class);
    }

    @Test
    public void shouldThrowExceptionDuringTurningNotExistingOrder() throws Exception {
        //given

        ClientData clientData = marek().assemble().generateSnapshot();
        //expect
        assertThatThrownBy(() -> {
            orderManagementService.turnOrder(clientData, "wrong_id");
        }).isInstanceOf(OrderItemTurnException.class).hasMessage("order does not exists");
    }

    @Test
    public void shouldThrowExceptionDuringTurningNotExistingItem() throws Exception {
        //given
        ClientData clientData = marek().assemble().generateSnapshot();
        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put(MOVIE_ID, 1L)
                .build();
        String orderId = orderManagementService.createOrder(clientData, "PLN", positions);


        //expect
        assertThatThrownBy(() -> {
            orderManagementService.turnOrderItem(clientData, orderId, "wrong_item_id");
        }).isInstanceOf(OrderItemTurnException.class).hasMessage("item to turn does not exists");
    }

    @Test
    public void shouldCreateOrderWithOneItem() throws Exception {
        //given

        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put(MOVIE_ID, 1L)
                .build();
        ClientData clientData = marek().assemble().generateSnapshot();
        //when
        String orderId = orderManagementService.createOrder(clientData, "PLN", positions);
        //then
        then(orderId).isNotEmpty();
    }

    @Test
    public void shouldTurnItemOfProperOrder() throws Exception {
        //given
        ClientData clientData = marek().assemble().generateSnapshot();
        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put(MOVIE_ID, 1L)
                .build();
        String orderId = orderManagementService.createOrder(clientData, "PLN", positions);
        String firstItemId = orderRepository.findByIdAndClientId(orderId, clientData.getId()).get().getItems().get(0).getId();
        //when
        OrderItemDto orderItemDto = orderManagementService.turnOrderItem(clientData, orderId, firstItemId);
        //then
        BDDAssertions.then(orderItemDto.getId()).isNotEmpty();
    }

    @Test
    public void shouldAddClientBonusTypeDuringTurnItem() throws Exception {
        Client client = clientRepository.save(marek().id(null).assemble());
        //given
        Map<String, Long> positions = new ImmutableMap.Builder<String, Long>()
                .put(MOVIE_ID, 1L)
                .build();
        ClientData clientData = client.generateSnapshot();
        String orderId = orderManagementService.createOrder(clientData, "PLN", positions);
        String itemId = orderManagementService.findClientOrderById(clientData.getId(), orderId).getItems().get(0).getId();
        //when
        orderManagementService.turnOrderItem(clientData, orderId, itemId);
        //then
        BDDAssertions.then(clientRepository.findOne(clientData.getId())).isNotNull();

    }
}

