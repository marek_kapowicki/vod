package pl.marekk.rental.application;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import ddd.support.domain.converter.BaseDtoConverter;
import ddd.support.query.PageRepresentation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.marekk.rental.domain.movie.Movie;
import pl.marekk.rental.domain.movie.MovieDto;
import pl.marekk.rental.domain.movie.MovieRepository;
import pl.marekk.rental.domain.movie.RequestedMovieDoesNotExists;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.apache.commons.collections.CollectionUtils.isEmpty;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

@Service
@Slf4j
public class MovieManagementService {

    private final MovieRepository movieRepository;
    private final BaseDtoConverter<Movie, MovieDto> dtoConverter;

    @Inject
    public MovieManagementService(MovieRepository movieRepository, BaseDtoConverter<Movie, MovieDto> dtoConverter) {
        this.movieRepository = movieRepository;
        this.dtoConverter = dtoConverter;
    }


    public MovieDto findById(String movieId) {
        log.info("finding movie by id={}", movieId);
        Optional<Movie> found = movieRepository.findById(movieId);
        Movie movie = found.orElseThrow(() -> new EntityNotFoundException());
        return dtoConverter.convert(movie);

    }

    public PageRepresentation<MovieDto> findAll(int page, int limit) {
        log.info("searching all movies page={}, limit={}", page, limit);
        Page<Movie> foundPage = movieRepository.findAll(new PageRequest(page, limit));
        if (!foundPage.hasContent()) {
            throw new EntityNotFoundException("empty movies collection");
        }
        return dtoConverter.convert(foundPage);
    }


    public List<Movie> findAllMoviesFromList(List<String> movieIds) {
        Preconditions.checkArgument(isNotEmpty(movieIds), "some movieId is needed");
        log.info("searching movies by ids={}", movieIds);
        Iterable<Movie> movies = movieRepository.findAll(movieIds);
        List<Movie> movieEntities = Lists.newArrayList(movies);
        checkSize(movieEntities, movieIds);
        return movieEntities;

    }

    private void checkSize(List<Movie> movies, List<String> ids) {
        if (isEmpty(movies) || movies.size() < ids.size()) {
            List<String> notExistingIds = notExistingIds(movies, ids);
            throw new RequestedMovieDoesNotExists(notExistingIds);
        }


    }

    private List<String> notExistingIds(List<Movie> movies, List<String> ids) {
        return ids.stream()
                .filter(movieId -> {
                    return movies.stream().noneMatch(movie -> movie.getId().equals(movieId));
                }).collect(Collectors.toList());
    }
}

