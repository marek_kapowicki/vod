package pl.marekk.rental.application;

import com.google.common.eventbus.EventBus;
import ddd.support.domain.converter.BaseDtoConverter;
import ddd.support.query.PageRepresentation;
import lombok.extern.slf4j.Slf4j;
import org.joda.money.CurrencyUnit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.marekk.rental.domain.client.AddBonusPointsEvent;
import pl.marekk.rental.domain.client.ClientBonusPointsListener;
import pl.marekk.rental.domain.client.ClientData;
import pl.marekk.rental.domain.client.order.*;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Service
@Slf4j
public class OrderManagementService {

    private final OrderFactory orderFactory;
    private final OrderRepository orderRepository;
    private final BaseDtoConverter<ClientOrder, OrderDto> converter;
    private final MovieManagementService movieManagementService;
    private EventBus eventBus = new EventBus();


    @Inject
    public OrderManagementService(OrderFactory orderFactory, OrderRepository orderRepository, BaseDtoConverter<ClientOrder, OrderDto> converter, MovieManagementService movieManagementService) {
        this.orderFactory = orderFactory;
        this.orderRepository = orderRepository;
        this.converter = converter;
        this.movieManagementService = movieManagementService;
        eventBus = new EventBus();
        eventBus.register(ClientBonusPointsListener.class);
    }

    public String createOrder(ClientData clientData, String currencyCode, Map<String, Long> positions) {
        log.info("creating order for client={} from positions={}", clientData, positions);
        final CurrencyUnit currencyUnit = CurrencyUnit.of(currencyCode);
        final List<OrderCreateParams.OrderCreatePositionParams> orderCreatePositionParams = buildOrderPositionParams(positions);
        ClientOrder order = orderFactory.create(new OrderCreateParams(clientData, currencyUnit, orderCreatePositionParams));
        return orderRepository.save(order).getId();
    }

    public OrderDto findClientOrderById(String clientId, String orderId) {
        log.info("searching client={} order={}", clientId, orderId);
        return converter.convert(orderRepository.findByIdAndClientId(orderId, clientId).orElseThrow(() ->
                new EntityNotFoundException()));
    }

    public PageRepresentation<OrderDto> findClientOrders(String clientId, int page, int limit) {
        log.info("searching client={} orders", clientId);
        Page<ClientOrder> orders = orderRepository
                .findByClientIdOrderByStartDateDesc(clientId, new PageRequest(page, limit));
        if (!orders.hasContent()) {
            throw new EntityNotFoundException();
        }
        return converter.convert(orders);
    }

    @Transactional
    public OrderItemDto turnOrderItem(ClientData clientData, String orderId, String orderItemId) {
        log.info("turning item={} from order={}", orderItemId, orderId);
        ClientOrder order = findOrderWithTurningItem(clientData, orderId);
        OrderItem item = extractOrderItem(orderItemId, order);

        OrderItemDto orderItemDto = item.turnItem();
        orderRepository.save(order);
        eventBus.post(new AddBonusPointsEvent(clientData.getId(), item.calculateClientBonusPoints()));
        return orderItemDto;

    }

    @Transactional
    public OrderDto turnOrder(ClientData clientData, String orderId) {
        log.info("turning items from order={}", orderId);
        ClientOrder order = findOrderWithTurningItem(clientData, orderId);
        order.getItems().stream()
                .forEach(it -> {
                    it.turnItem();
                    eventBus.post(new AddBonusPointsEvent(clientData.getId(), it.calculateClientBonusPoints()));
                });
        orderRepository.save(order);
        return converter.convert(order);

    }

    private ClientOrder findOrderWithTurningItem(ClientData clientData, String orderId) {
        return orderRepository.findByIdAndClientId(orderId, clientData.getId())
                .orElseThrow(() -> new OrderItemTurnException("order does not exists"));
    }

    private OrderItem extractOrderItem(String orderItemId, ClientOrder order) {
        return order.getItems().stream()
                .filter(it -> orderItemId.equals(it.getId())).findFirst()
                .orElseThrow(() -> new OrderItemTurnException("item to turn does not exists"));
    }

    private List<OrderCreateParams.OrderCreatePositionParams> buildOrderPositionParams(Map<String, Long> positions) {
        List<String> movieIds = positions.keySet().stream()
                .collect(toList());

        return movieManagementService.findAllMoviesFromList(movieIds).stream()
                .map(it -> new OrderCreateParams.OrderCreatePositionParams(it, positions.get(it.getId())))
                .collect(toList());


    }


}
