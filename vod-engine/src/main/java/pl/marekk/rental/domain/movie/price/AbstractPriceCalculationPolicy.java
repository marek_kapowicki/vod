package pl.marekk.rental.domain.movie.price;

import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Slf4j
abstract class AbstractPriceCalculationPolicy implements PriceCalculationPolicy {

    static final BigDecimal BASIC_PRICE = BigDecimal.valueOf(30);


    private final PriceCalculator priceCalculator = new PriceCalculator();
    private final BigDecimal periodUnitPrice;
    private final int periodUnitDurationInDays;

    @Inject
    public AbstractPriceCalculationPolicy(BigDecimal periodUnitPrice, int periodUnitDurationInDays) {
        this.periodUnitPrice = periodUnitPrice;
        this.periodUnitDurationInDays = periodUnitDurationInDays;
    }

    @Override
    public BigDecimal calculate(@Min(1) long durationInDays) {
        Preconditions.checkArgument(durationInDays >= 1);
        log.info("calculating price for movie and duration={}", durationInDays);
        return priceCalculator.calculate(periodUnitPrice, periodUnitDurationInDays, durationInDays);
    }


    @Override
    public BigDecimal calculateCharge(@Min(0) long durationInDays) {
        log.info("calculating charge for duration={} days delay", durationInDays);
        return periodUnitPrice.multiply(BigDecimal.valueOf(durationInDays));
    }
}
