package pl.marekk.rental.domain.client.order;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import ddd.support.query.BaseDto;
import lombok.Getter;
import org.joda.money.Money;

import java.time.LocalDate;

@Getter
public class OrderItemDto implements BaseDto {

    private final String id;
    private final Money additionalPrice;
    private final Money initialPrice;
    private final LocalDate initialReturnDate;
    private final LocalDate finalReturnDate;
    private final String movieName;


    @JsonCreator
    public OrderItemDto(@JsonProperty("id") String id,
                        @JsonProperty("additionalPrice") Money additionalPrice,
                        @JsonProperty("initialPrice") Money initialPrice,
                        @JsonProperty("initialReturnDate") LocalDate initialReturnDate,
                        @JsonProperty("finalReturnDate") LocalDate finalReturnDate,
                        @JsonProperty("movieName") String movieName) {
        this.id = id;
        this.additionalPrice = additionalPrice;
        this.initialPrice = initialPrice;
        this.initialReturnDate = initialReturnDate;
        this.finalReturnDate = finalReturnDate;
        this.movieName = movieName;
    }
}
