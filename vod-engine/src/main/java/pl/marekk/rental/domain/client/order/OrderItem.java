package pl.marekk.rental.domain.client.order;

import ddd.support.domain.BaseEntity;
import ddd.support.domain.ConvertibleToDto;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import pl.marekk.rental.domain.movie.MovieData;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import static java.util.Optional.ofNullable;

@Entity
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
@Getter(AccessLevel.PACKAGE)
public class OrderItem extends BaseEntity implements ConvertibleToDto<OrderItemDto> {

    @Embedded
    @NotNull
    @NonNull
    @Column(nullable = false)
    private MovieData movie;

    @NotNull
    @NonNull
    @Column(nullable = false)
    private LocalDate startDate;

    @NotNull
    @Column(nullable = false)
    private LocalDate initialReturnDate;

    private LocalDate finalReturnDate;

    @NotNull
    @NonNull
    @Column(nullable = false)
    private Money initialPrice;
    private Money totalPrice;

    public OrderItem(MovieData movieData, CurrencyUnit currency, LocalDate startDate, LocalDate initialReturnDate) {
        this.movie = movieData;
        this.startDate = startDate;
        this.initialReturnDate = initialReturnDate;

        BigDecimal price = movie.calculateRentalInitPrice(ChronoUnit.DAYS.between(startDate, initialReturnDate));

        this.initialPrice = Money.of(currency, price);
    }

    @Override
    public OrderItemDto buildDto() {
        log.info("building dto from item={}", this);
        Money additionalPrice = calculateAdditionalPrice(totalPrice, initialPrice);
        return new OrderItemDto(getId(), additionalPrice, initialPrice, initialReturnDate, finalReturnDate, movie.getName());
    }

    public OrderItemDto turnItem() {
        LocalDate now = LocalDate.now();

        long durationInDays = ChronoUnit.DAYS.between(initialReturnDate, now);
        log.info("calculating price for item={}] ", this);

        BigDecimal additionalCharge = movie.calculateRentalCharge(durationInDays);

        totalPrice = initialPrice.plus(additionalCharge);
        log.info("returning item,duration={} totalPrice={}", durationInDays, totalPrice);
        if (isReturned()) {
            throw new ItemAlreadyReturnedException();
        }
        this.finalReturnDate = now;
        return buildDto();
    }

    public int calculateClientBonusPoints() {
        log.info("calculating bonus points for item={}", getId());
        return movie.calculateBonusPoints();
    }

    private Money calculateAdditionalPrice(Money totalPrice, Money initialPrice) {
        log.info("calculating different between totalPrice={} snd initialPrice={}", totalPrice, initialPrice);
        return ofNullable(totalPrice)
                .map(it -> it.minus(initialPrice))
                .orElse(Money.zero(initialPrice.getCurrencyUnit()));
    }

    private boolean isReturned() {
        return finalReturnDate != null && totalPrice != null;
    }
}
