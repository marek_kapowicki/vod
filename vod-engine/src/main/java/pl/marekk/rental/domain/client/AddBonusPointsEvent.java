package pl.marekk.rental.domain.client;

import lombok.Value;

@Value
public class AddBonusPointsEvent {
    private String clientId;
    private int bonusPoints;
}
