package pl.marekk.rental.domain.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import javax.inject.Named;
import java.time.LocalDateTime;

@Named
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
@Slf4j
public class ClientContext {

    private ClientData clientData;


    public ClientData getClient() {
        return new ClientData("someId", "marek", LocalDateTime.now());
    }
}
