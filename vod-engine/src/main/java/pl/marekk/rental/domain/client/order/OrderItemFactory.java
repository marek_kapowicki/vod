package pl.marekk.rental.domain.client.order;

import lombok.extern.slf4j.Slf4j;
import org.joda.money.CurrencyUnit;
import pl.marekk.rental.domain.movie.Movie;
import pl.marekk.rental.domain.movie.MovieData;

import javax.inject.Named;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

import static com.google.common.base.Preconditions.checkArgument;

@Named
@Slf4j
public class OrderItemFactory {

    public OrderItem create(@NotNull Movie movie,
                            @NotNull CurrencyUnit currency,
                            @Min(0) long durationInDays) {
        checkArgument(movie != null);
        checkArgument(currency != null);
        checkArgument(durationInDays >= 0);
        MovieData movieData = movie.generateSnapshot();
        log.info("creating new order item  [movieData={}, durationInDays={}]", movieData, durationInDays);
        LocalDate startDate = LocalDate.now();
        LocalDate returnDate = startDate.plusDays(durationInDays);
        return new OrderItem(movieData, currency, startDate, returnDate);
    }

}
