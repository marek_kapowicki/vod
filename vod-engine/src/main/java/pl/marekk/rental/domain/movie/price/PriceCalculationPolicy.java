package pl.marekk.rental.domain.movie.price;

import java.math.BigDecimal;

public interface PriceCalculationPolicy {
    BigDecimal calculate(long durationInDays);

    BigDecimal calculateCharge(long durationInDays);
}
