package pl.marekk.rental.domain.client.order;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderRepository extends PagingAndSortingRepository<ClientOrder, String>, JpaSpecificationExecutor<ClientOrder> {
    Optional<ClientOrder> findByIdAndClientId(String id, String clientId);

    Page<ClientOrder> findByClientIdOrderByStartDateDesc(String clientId, Pageable pageable);

}
