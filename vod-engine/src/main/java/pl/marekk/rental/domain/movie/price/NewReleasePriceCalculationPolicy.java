package pl.marekk.rental.domain.movie.price;

import java.math.BigDecimal;


public class NewReleasePriceCalculationPolicy extends AbstractPriceCalculationPolicy {

    static final BigDecimal PREMIUM_PRICE = BigDecimal.valueOf(40);

    static final int NEW_POLICY_PERIOD_DURATION = 1;

    public NewReleasePriceCalculationPolicy() {
        super(PREMIUM_PRICE, NEW_POLICY_PERIOD_DURATION);
    }
}
