package pl.marekk.rental.domain.client.order;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.joda.money.CurrencyUnit;
import pl.marekk.rental.domain.client.ClientData;
import pl.marekk.rental.domain.movie.Movie;

import java.util.List;

@Getter(AccessLevel.PROTECTED)
@ToString
@AllArgsConstructor
@Slf4j
public class OrderCreateParams {
    private ClientData clientData;
    private CurrencyUnit currency;
    private List<OrderCreatePositionParams> itemsCreateParams;

    public void checkParams() {
        log.info("checking validation of params={}", this);
        if (!isValid()) {
            throw new OrderCreateException();
        }
    }

    private boolean isValid() {
        boolean itemsCreateParamsValid =
                itemsCreateParams != null &&
                        itemsCreateParams.stream()
                                .map(OrderCreatePositionParams::isValid)
                                .reduce(true, (it1, it2) -> it1 && it2);
        return clientData != null && currency != null && itemsCreateParamsValid;
    }

    @AllArgsConstructor
    @Getter
    public static class OrderCreatePositionParams {

        private Movie movie;
        private long durationInDays;

        public boolean isValid() {
            return movie != null && durationInDays > 0;
        }

    }

}
