package pl.marekk.rental.domain.client.order;

public class ItemAlreadyReturnedException extends RuntimeException {
    public ItemAlreadyReturnedException() {
        super("item has been already returned");
    }
}
