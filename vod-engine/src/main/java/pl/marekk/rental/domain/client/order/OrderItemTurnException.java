package pl.marekk.rental.domain.client.order;

public class OrderItemTurnException extends RuntimeException {

    public OrderItemTurnException(String message) {
        super(message);
    }
}
