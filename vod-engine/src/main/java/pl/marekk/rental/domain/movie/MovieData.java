package pl.marekk.rental.domain.movie;

import ddd.support.domain.EntitySnapshot;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Embeddable
@Getter
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class MovieData implements EntitySnapshot<Movie> {
    @NotNull
    @lombok.NonNull
    @Column(name = "movie_id", nullable = false)
    private String id;

    @NotNull
    @lombok.NonNull
    @Column(nullable = false)
    private String name;

    @NotNull
    @lombok.NonNull
    @Column(nullable = false)
    private MovieCategory category;

    @NotNull
    @lombok.NonNull
    @Column(nullable = false)
    private LocalDateTime snapshotDate;


    public int calculateBonusPoints() {
        return category.calculateBonusPoints();
    }


    public BigDecimal calculateRentalInitPrice(long durationInDays) {

        return category.getPriceCalculationPolicy().calculate(durationInDays == 0 ? 1 : durationInDays);
    }

    public BigDecimal calculateRentalCharge(long durationInDays) {

        return category.getPriceCalculationPolicy().calculateCharge(durationInDays < 0 ? 0 : durationInDays);
    }

}
