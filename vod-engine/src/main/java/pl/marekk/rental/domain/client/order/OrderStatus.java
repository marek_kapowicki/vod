package pl.marekk.rental.domain.client.order;

enum OrderStatus {
    IN_PROGRESS, CLOSED
}
