package pl.marekk.rental.domain.movie;

import ddd.support.query.BaseDto;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;

@Value
@Slf4j
public class MovieDto implements BaseDto {

    private static final long serialVersionUID = -3619376976610649006L;
    private final String name;
    private final String description;
    private final String category;
    private final String grade;
    private final LocalDate premiere;
    private final String id;

    @Override
    public String getId() {
        return id;
    }
}
