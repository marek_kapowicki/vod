package pl.marekk.rental.domain.movie.price;

public class RegularPriceCalculationPolicy extends AbstractPriceCalculationPolicy {

    static final int REGULAR_POLICY_PERIOD_DURATION = 3;

    public RegularPriceCalculationPolicy() {
        super(BASIC_PRICE, REGULAR_POLICY_PERIOD_DURATION);
    }
}
