package pl.marekk.rental.domain.client.order;

public class OrderCreateException extends RuntimeException {
    public OrderCreateException() {
        super("some problems during create order");
    }
}
