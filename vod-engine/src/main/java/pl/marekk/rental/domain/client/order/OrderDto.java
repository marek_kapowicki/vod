package pl.marekk.rental.domain.client.order;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import ddd.support.query.BaseDto;
import lombok.Getter;
import org.joda.money.Money;

import java.time.LocalDate;
import java.util.List;

@Getter
public class OrderDto implements BaseDto {
    private final String id;
    private final LocalDate startDate;
    private final Money additionalPrice;
    private final Money initialPrice;
    private final LocalDate endDate;
    private final List<OrderItemDto> items;
    private final String orderStatus;

    @JsonCreator
    public OrderDto(@JsonProperty("id") String id,
                    @JsonProperty("startDate") LocalDate startDate,
                    @JsonProperty("additionalPrice") Money additionalPrice,
                    @JsonProperty("initialPrice") Money initialPrice,
                    @JsonProperty("endDate") LocalDate endDate,
                    @JsonProperty("items") List<OrderItemDto> items,
                    @JsonProperty("orderStatus") String orderStatus) {
        this.id = id;
        this.startDate = startDate;
        this.additionalPrice = additionalPrice;
        this.initialPrice = initialPrice;
        this.endDate = endDate;
        this.items = items;
        this.orderStatus = orderStatus;
    }

    @Override
    public String getId() {
        return id;
    }


}
