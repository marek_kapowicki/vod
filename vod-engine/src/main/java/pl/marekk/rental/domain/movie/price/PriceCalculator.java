package pl.marekk.rental.domain.movie.price;

import javax.inject.Named;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import static com.google.common.base.Preconditions.checkArgument;

@Named
class PriceCalculator {
    BigDecimal calculate(@NotNull BigDecimal unitPrice, @Min(0) long periodUnitDuration, @Min(1) long duration) {
        checkArgument(unitPrice != null, "unit price can not be null");
        checkArgument(periodUnitDuration > 0, "unit duration must be greater then 0");
        checkArgument(duration > 0, "duration must be greater then 0");
        if (duration <= periodUnitDuration) {
            return unitPrice;
        }
        long delayInDays = duration - periodUnitDuration;
        BigDecimal fee = unitPrice.multiply(BigDecimal.valueOf(delayInDays));
        return unitPrice.add(fee);
    }
}
