package pl.marekk.rental.domain.movie;

enum MovieGrade {
    CRIME, ACTION, COMEDY, FAMILY, DRAMA, OTHER;
}
