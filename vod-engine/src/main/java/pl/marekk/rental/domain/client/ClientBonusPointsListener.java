package pl.marekk.rental.domain.client;

import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Named;

@Slf4j
@Named
public class ClientBonusPointsListener {

    private final ClientRepository clientRepository;


    @Inject
    public ClientBonusPointsListener(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Subscribe
    public void listen(AddBonusPointsEvent event) {
        log.info("receiving AddBonusPointsEvent={}", event);
        Client client = clientRepository.findOne(event.getClientId());
        client.addBonusPoints(event.getBonusPoints());


    }
}
