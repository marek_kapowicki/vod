package pl.marekk.rental.domain.movie;

import java.util.List;

public class RequestedMovieDoesNotExists extends RuntimeException {
    public RequestedMovieDoesNotExists(List<String> message) {
        super(String.format("requested ids=%s does not exists", message));
    }
}
