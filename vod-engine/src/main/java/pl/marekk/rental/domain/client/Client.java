package pl.marekk.rental.domain.client;

import ddd.support.domain.BaseEntity;
import ddd.support.domain.ConvertibleToSnapshot;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Slf4j
@Getter(AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Client extends BaseEntity implements ConvertibleToSnapshot<ClientData> {

    @NotNull
    @Column(unique = true, nullable = false)
    private String email;

    private String password;

    @Column(nullable = false)
    private int bonusPoints = 0;

    protected void addBonusPoints(int points) {
        bonusPoints += points;
    }


    @Override
    public ClientData generateSnapshot() {
        return new ClientData(getId(), email, LocalDateTime.now());
    }
}
