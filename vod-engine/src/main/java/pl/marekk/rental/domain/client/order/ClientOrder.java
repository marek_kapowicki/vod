package pl.marekk.rental.domain.client.order;

import com.google.common.collect.Lists;
import ddd.support.domain.BaseEntity;
import ddd.support.domain.ConvertibleToDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import pl.marekk.rental.domain.client.ClientData;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ddd.support.domain.converter.BaseDtoConverter.ConversionHelper.convertEnum;

@Entity()
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Getter(AccessLevel.PUBLIC)
@Slf4j
public class ClientOrder extends BaseEntity implements ConvertibleToDto<OrderDto> {

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @Fetch(FetchMode.JOIN)
    @OrderColumn(name = "itemNumber")
    @JoinColumn(name = "order_id")
    private List<OrderItem> items = Lists.newArrayList();

    @Embedded
    @NotNull
    private ClientData client;

    @NotNull
    @Column(nullable = false)
    private LocalDate startDate;

    @Override
    public OrderDto buildDto() {
        log.info("converting order={} to dto", this);
        CurrencyUnit dtoCurrency = items.get(0).getInitialPrice().getCurrencyUnit();
        Money dtoInitialPrice = calculateInitialPrice(dtoCurrency);
        String dtoOrderStatus = convertEnum(calculateStatus());
        LocalDate dtoEndDate = null;
        Money dtoAdditionalPrice = calculateAdditionalPrice(dtoCurrency);

        if (OrderStatus.CLOSED.name().equals(dtoOrderStatus)) {
            dtoEndDate = calculateOrderEndDate();
        }
        List<OrderItemDto> dtoItems = items.stream().map(it -> it.buildDto()).collect(Collectors.toList());
        return new OrderDto(getId(), startDate, dtoAdditionalPrice, dtoInitialPrice,
                dtoEndDate, dtoItems, dtoOrderStatus);
    }

    private Money calculateAdditionalPrice(CurrencyUnit currency) {
        log.info("calculating order={} additional price", getId());
        return items.stream()
                .filter(it -> it.getFinalReturnDate() != null)
                .map(it -> it.getTotalPrice().minus(it.getInitialPrice()))
                .reduce(Money.zero(currency), Money::plus);
    }


    private Money calculateInitialPrice(CurrencyUnit currency) {
        log.info("calculating order={} total price", getId());
        return items.stream()
                .map(OrderItem::getInitialPrice)
                .reduce(Money.zero(currency), Money::plus);
    }


    private LocalDate calculateOrderEndDate() {
        log.info("calculating order={} end date", getId());
        return items.stream()
                .map(OrderItem::getFinalReturnDate)
                .filter(it -> it != null)
                .max(Comparator.comparing(it -> it)).orElse(null);
    }

    private OrderStatus calculateStatus() {
        log.info("calculating order={} status", getId());
        long returnedItemsCount = countReturnedItems();
        return (returnedItemsCount < items.size()) ? OrderStatus.IN_PROGRESS : OrderStatus.CLOSED;
    }

    private long countReturnedItems() {
        return items.stream()
                .map(OrderItem::getFinalReturnDate)
                .filter(it -> it != null)
                .count();
    }

}
