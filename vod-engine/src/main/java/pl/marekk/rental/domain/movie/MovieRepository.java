package pl.marekk.rental.domain.movie;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MovieRepository extends PagingAndSortingRepository<Movie, String>, JpaSpecificationExecutor<Movie> {
    Optional<Movie> findById(String id);

}
