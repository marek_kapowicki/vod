package pl.marekk.rental.domain.client.order;

import lombok.extern.slf4j.Slf4j;
import org.joda.money.CurrencyUnit;
import pl.marekk.rental.domain.client.ClientData;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;

@Named
@Slf4j
public class OrderFactory {

    private final OrderItemFactory orderItemFactory;

    @Inject
    public OrderFactory(OrderItemFactory orderItemFactory) {
        this.orderItemFactory = orderItemFactory;
    }

    public ClientOrder create(@NotNull OrderCreateParams orderCreateParams) {
        checkArgument(orderCreateParams != null);
        orderCreateParams.checkParams();
        final LocalDate startDate = LocalDate.now();
        log.info("creating order from params={}, startDate={}", orderCreateParams, startDate);
        final ClientData client = orderCreateParams.getClientData();
        final CurrencyUnit currency = orderCreateParams.getCurrency();

        List<OrderItem> orderItems = orderCreateParams.getItemsCreateParams()
                .stream()
                .map(it -> orderItemFactory.create(it.getMovie(), currency, it.getDurationInDays()))
                .collect(Collectors.toList());
        return new ClientOrder(orderItems, client, startDate);
    }
}
