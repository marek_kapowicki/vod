package pl.marekk.rental.domain.movie;

import pl.marekk.rental.domain.movie.price.NewReleasePriceCalculationPolicy;
import pl.marekk.rental.domain.movie.price.OldPriceCalculationPolicy;
import pl.marekk.rental.domain.movie.price.PriceCalculationPolicy;
import pl.marekk.rental.domain.movie.price.RegularPriceCalculationPolicy;

import java.util.function.Supplier;

enum MovieCategory {

    NEW_RELEASE(new NewReleasePriceCalculationPolicy(), () -> 2),
    REGULAR(new RegularPriceCalculationPolicy(), () -> 1),
    OLD(new OldPriceCalculationPolicy(), () -> 1);

    public PriceCalculationPolicy getPriceCalculationPolicy() {
        return priceCalculationPolicy;
    }

    protected int calculateBonusPoints() {
        return bonusPointsCalculationPolicy.get();
    }

    private final PriceCalculationPolicy priceCalculationPolicy;
    private final Supplier<Integer> bonusPointsCalculationPolicy;

    private MovieCategory(PriceCalculationPolicy priceCalculationPolicy, Supplier<Integer> bonusPointsCalculationPolicy) {
        this.priceCalculationPolicy = priceCalculationPolicy;
        this.bonusPointsCalculationPolicy = bonusPointsCalculationPolicy;
    }


}
