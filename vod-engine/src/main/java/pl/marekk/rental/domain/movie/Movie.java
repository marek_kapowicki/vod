package pl.marekk.rental.domain.movie;

import ddd.support.domain.BaseEntity;
import ddd.support.domain.ConvertibleToDto;
import ddd.support.domain.ConvertibleToSnapshot;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static ddd.support.domain.converter.BaseDtoConverter.ConversionHelper.convertEnum;

@Entity
@Slf4j
@Getter(AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Movie extends BaseEntity implements ConvertibleToDto<MovieDto>, ConvertibleToSnapshot<MovieData> {

    @Column(unique = true, nullable = false)
    @NotNull
    @NonNull
    private String name;

    @NotNull
    @NonNull
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private MovieCategory category;

    private LocalDate premiere;

    private String description;

    @Enumerated(value = EnumType.STRING)
    private MovieGrade grade;

    @Override
    public MovieDto buildDto() {
        log.info("convert movie={} to dto", this);
        final String dtoCategory = convertEnum(category);
        final String dtoGrade = convertEnum(grade);
        return new MovieDto(name, description, dtoCategory, dtoGrade, premiere, getId());

    }

    @Override
    public MovieData generateSnapshot() {
        log.info("generating movie={} snapshot", this);
        return new MovieData(getId(), name, category, LocalDateTime.now());
    }

}
