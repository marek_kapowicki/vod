package pl.marekk.rental.domain.movie.price;

public class OldPriceCalculationPolicy extends AbstractPriceCalculationPolicy {


    static final int OLD_POLICY_PERIOD_DURATION = 5;

    public OldPriceCalculationPolicy() {
        super(BASIC_PRICE, OLD_POLICY_PERIOD_DURATION);
    }
}
