package configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@Import(RepositoryConfiguration.class)
@ComponentScan("pl.marekk.rental.domain.client")
@EnableJpaAuditing
public class ClientDomainConfiguration {
}
