package configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({MovieDomainConfiguration.class, ClientDomainConfiguration.class})
@ComponentScan("pl.marekk.rental.application")
public class MainDomainConfiguration {
}
