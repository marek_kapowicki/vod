package configuration;

import org.springframework.boot.autoconfigure.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableJpaRepositories(basePackages = {RepositoryConfiguration.CLIENT_ENTITY_SCAN, RepositoryConfiguration.MOVIE_ENTITY_SCAN})
@EntityScan(basePackages = {RepositoryConfiguration.DOMAIN_SUPPORT_ENTITY_SCAN, RepositoryConfiguration.CLIENT_ENTITY_SCAN, RepositoryConfiguration.MOVIE_ENTITY_SCAN})
@EnableTransactionManagement
@ComponentScan(RepositoryConfiguration.DOMAIN_SUPPORT_ENTITY_SCAN)
public class RepositoryConfiguration {

    public static final String MOVIE_ENTITY_SCAN = "pl.marekk.rental.domain.movie";
    public static final String CLIENT_ENTITY_SCAN = "pl.marekk.rental.domain.client";
    public static final String DOMAIN_SUPPORT_ENTITY_SCAN = "ddd.support.domain";

    @Bean
    public DataSource dataSource() {
        EmbeddedDatabaseBuilder databaseBuilder = new EmbeddedDatabaseBuilder();
        return databaseBuilder.setType(EmbeddedDatabaseType.H2).build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        EntityManagerFactoryBuilder builder = new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(), hibernatePropertiesMap(), null);
        return builder.dataSource(dataSource).packages(MOVIE_ENTITY_SCAN, CLIENT_ENTITY_SCAN).build();

    }

    @Bean
    public PlatformTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory.getObject());
    }

    private JpaProperties hibernatePropertiesMap() {
        JpaProperties jpaProperties = new JpaProperties();
        Map<String, String> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "create-drop");
        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.show_sql", "true");
        jpaProperties.setProperties(properties);
        return jpaProperties;
    }

}
