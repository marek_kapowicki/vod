package ddd.support.domain;

import java.time.LocalDateTime;

public interface EntitySnapshot<T extends BaseEntity> {
    String getId();

    LocalDateTime getSnapshotDate();
}
