package ddd.support.domain;

import ddd.support.query.BaseDto;

public interface ConvertibleToDto<T extends BaseDto> {
    public T buildDto();
}
