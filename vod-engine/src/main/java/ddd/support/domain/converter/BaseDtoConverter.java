package ddd.support.domain.converter;

import com.google.common.collect.Lists;
import ddd.support.domain.ConvertibleToDto;
import ddd.support.query.BaseDto;
import ddd.support.query.PageRepresentation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;

import javax.inject.Named;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;


@Slf4j
@Named
public class BaseDtoConverter<T extends ConvertibleToDto<Y>, Y extends BaseDto> {

    public Y convert(T entity) {
        checkArgument(entity != null);
        return entity.buildDto();
    }

    public PageRepresentation<Y> convert(Page<T> page) {
        checkArgument(page.hasContent());
        log.info("convert movie page={} to dtos", page);
        List<Y> dtos = Lists.newArrayList();
        page.forEach(e -> dtos.add(convert(e)));
        return new PageRepresentation<>(page.getTotalPages(), dtos);

    }

    public static final class ConversionHelper {

        public static String convertEnum(Enum someEnum) {
            return Optional.ofNullable(someEnum).map(Enum::name).orElse(null);
        }
    }
}
