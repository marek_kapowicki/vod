package ddd.support.domain;

public interface ConvertibleToSnapshot<T extends EntitySnapshot> {
    T generateSnapshot();
}
