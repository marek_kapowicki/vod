package ddd.support.query;

import lombok.Value;

import java.util.List;

@Value
public class PageRepresentation<T extends BaseDto> {
    private Integer totalPages;
    private List<T> items;


}
