package ddd.support.query;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Value;

@Getter
@Value
public class IdResponse implements BaseResponse {
    private String id;

    @JsonCreator
    public IdResponse(@JsonProperty("id") String id) {
        this.id = id;
    }
}
