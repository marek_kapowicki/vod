package ddd.support.query;

import java.io.Serializable;

public interface BaseResponse extends Serializable {
    String getId();
}
